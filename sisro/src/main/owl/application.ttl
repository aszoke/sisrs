@prefix : <http://purl.org/net/sisr/owl/application#> .
@prefix dc: <http://purl.org/dc/elements/1.1/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix xml: <http://www.w3.org/XML/1998/namespace> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix dedo: <http://purl.org/net/sisr/owl/dedo#> .
@base <http://purl.org/net/sisr/owl/application#> .

<http://purl.org/net/sisr/owl/application> rdf:type owl:Ontology ;
	dc:creator "Szőke, Ákos" ;
	dc:creator "Förhécz, András" ;
	dc:title "Application Description Ontology (ADO)"@en ;	
	dc:title "Alkalmazás leíró ontológia (ADO)"@hu ;
	dc:description "This ontology can be used to describe applications that run on devices." ;
	rdfs:comment """Application Description Ontology (ADO) for Sensor Instance Semantic Repository (SISR)
	Revision 1.0
	date 2014.03.30
	basic temporal vocab
	"""@en ;
    rdfs:comment "The application ontology describes softwares that run on devices."@en ;
	owl:versionInfo "1.0" ;
	dc:language "en" ;
	dc:date "2014.03.30" .

### C L A S S E S ###

:ApplicationThing rdf:type owl:Class ;
	rdfs:label "Application thing"@en , "Alkalmazás dolog"@hu ; 
	rdfs:subClassOf owl:Thing ;
	rdfs:comment "Description of an application."@en .

:Application rdf:type owl:Class ; 
	rdfs:label "Application"@en , "Alkalmazás"@hu ;
	owl:sameAs dedo:Software ;
	rdfs:subClassOf :ApplicationThing ;
	rdfs:comment "Application that runs on a device."@en .

### O B J E C T    P R O P E R T I E S ###

:runsOn rdf:type owl:ObjectProperty ;      
	rdfs:range dedo:Device ;
	rdfs:domain :Application .

### D A T A   P R O P E R T I E S ###

:data-property 
	rdfs:label "Application data property"@en , "Alkalmazás adat tulajdonság"@hu ;
	rdf:type owl:DatatypeProperty .

:memory-description
	rdf:subPropertyOf :data-property ;
	rdfs:label "Memory Description"@en , "Memória leírás"@hu .

:memory-requirement
	rdf:type owl:DatatypeProperty ;
	rdfs:subPropertyOf :memory-description ;
	rdf:type owl:FunctionalProperty ;
	rdfs:label "Memory requirement"@en , "Memória követelmény"@hu ;
	rdfs:range xsd:double ;
	rdfs:comment "The memory capacity requirement against the device."@en .

:memory-usage
	rdf:type owl:DatatypeProperty ;
	rdfs:subPropertyOf :memory-description ;
	rdf:type owl:FunctionalProperty ;
	rdfs:label "Memory usage"@en , "Memória használat"@hu ;
	rdfs:range xsd:double ;
	rdfs:comment "The memory usage on the device."@en .
	
:memory-usage-unit
	rdf:type owl:DatatypeProperty ;
	rdfs:subPropertyOf :memory-description ;
	rdf:type owl:FunctionalProperty ;
	rdfs:label "Memory usage unit"@en , "Memória használat mértékének egysége"@hu ;
	rdfs:range xsd:token ;
	rdfs:comment "The memory usage unit on the device."@en .

:storage-description
	rdf:subPropertyOf :data-property ;
	rdfs:label "Memory Description"@en , "Memória leírás"@hu .

:storage-requirement
	rdf:type owl:DatatypeProperty ;
	rdfs:subPropertyOf :memory-description ;
	rdf:type owl:FunctionalProperty ;
	rdfs:label "Storage requirement"@en , "Tároló követelmény"@hu ;
	rdfs:range xsd:double ;
	rdfs:comment "The storage capacity requirement against the device."@en .

:storage-usage
	rdf:type owl:DatatypeProperty ;
	rdfs:subPropertyOf :storage-description ;
	rdf:type owl:FunctionalProperty ;
	rdfs:label "Storage usage"@en , "Tároló használat"@hu ;
	rdfs:range xsd:double ;
	rdfs:comment "The storage usage."@en .
	
:storage-usage-unit
	rdf:type owl:DatatypeProperty ;
	rdfs:subPropertyOf :storage-description ;
	rdf:type owl:FunctionalProperty ;
	rdfs:label "Storage usage unit"@en , "Tároló használat egység"@hu ;
	rdfs:range xsd:token ;
	rdfs:comment "The unit used to express the usage of storage."@en .
	
:cpu-description
	rdf:subPropertyOf :data-property ;
	rdfs:label "CPU Description"@en , "Processzor leírás"@hu .

:cpu-computing-power-requirement
	rdf:type owl:DatatypeProperty ;
	rdfs:subPropertyOf :cpu-description ;
	rdf:type owl:FunctionalProperty ;
	rdfs:label "CPU computing power requirement"@en , "CPU számítási teljesítmény követelmény"@hu ;
	rdfs:range xsd:double ;
	rdfs:comment "The CPU capacity requirement against the device."@en .

:cpu-computing-power-usage
	rdf:type owl:DatatypeProperty ;
	rdfs:subPropertyOf :cpu-description ;
	rdf:type owl:FunctionalProperty ;
	rdfs:label "CPU computing power usage"@en , "CPU számítási kapacitás használat"@hu ;
	rdfs:range xsd:double ;
	rdfs:comment "The usage of CPU computing power."@en .
	
:cpu-computing-power-usage-unit
	rdf:type owl:DatatypeProperty ;
	rdfs:subPropertyOf :cpu-description ;
	rdf:type owl:FunctionalProperty ;
	rdfs:label "CPU computing power unit"@en , "Számítási teljesítmény egység"@hu ;
	rdfs:range xsd:token ;
	rdfs:comment "The computing power unit of a CPU."@en .

:execution-description
	rdf:subPropertyOf :data-property ;
	rdfs:label "Execution parameters description"@en , "Futtatási paraméterek leírás"@hu .

:execution-zip-url
	rdfs:subPropertyOf :execution-description ;
	rdf:type owl:FunctionalProperty ;
	rdfs:label "CPU computing power unit"@en , "Számítási teljesítmény egység"@hu ;
	rdfs:range xsd:string ;
	rdfs:comment "The computing power unit of a CPU."@en .

:execution-main-exe
	rdfs:subPropertyOf :execution-description ;
	rdf:type owl:FunctionalProperty ;
	rdfs:label "Main executable of the application"@en , "Az alkalmazás futtatandó programja"@hu ;
	rdfs:range xsd:string ;
	rdfs:comment "The computing power unit of a CPU."@en .

:execution-exe-type
	rdfs:subPropertyOf :execution-description ;
	rdf:type owl:FunctionalProperty ;
	rdfs:label "Executable type"@en , "Futtatandó program típusa"@hu ;
	rdfs:range xsd:string ;
	rdfs:comment "The type of the main executable (e.g. 'java/jar')."@en .

:execution-cmd-args
	rdfs:subPropertyOf :execution-description ;
	rdf:type owl:FunctionalProperty ;
	rdfs:label "Command line arguments that the main executable receives."@en , "Parancssori paraméterek, amelyekkel az alkalmazás meghívódik."@hu ;
	rdfs:range xsd:string ;
	rdfs:comment "Command line arguments of the executable."@en .

:execution-db-url
	rdfs:subPropertyOf :execution-description ;
	rdf:type owl:FunctionalProperty ;
	rdfs:label "Application's data database URL."@en , "Az alkalmazás adatok adatbázisának az URL-je."@hu ;
	rdfs:range xsd:string ;
	rdfs:comment "Users can persist their application data in a database to which they receive user specific accounts, this is the URL of that database."@en .

:execution-db-user
	rdfs:subPropertyOf :execution-description ;
	rdf:type owl:FunctionalProperty ;
	rdfs:label "Username for the application-persistence database."@en , "Az alkalmazás adatok adatbázisának a felhasználói neve."@hu ;
	rdfs:range xsd:string ;
	rdfs:comment "User of the database."@en .

:execution-db-password
	rdfs:subPropertyOf :execution-description ;
	rdf:type owl:FunctionalProperty ;
	rdfs:label "Password for the application-persistence database."@en , "Az alkalmazás adatok adatbázisának a felhasználói jelszava."@hu ;
	rdfs:range xsd:string ;
	rdfs:comment "Password of the database user."@en .