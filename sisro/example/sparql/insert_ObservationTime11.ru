PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX tio: <http://purl.org/net/sisr/owl/time#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX : <http://localhost:5820/sisro/data#>

# Inserting observation time instance
INSERT DATA
{ 
:TimeInstant-11 rdf:type tio:TimeInstant ; # the URI (i.e. ':TimeInstant-11') must be unique, it identifies a time instance
	rdf:type owl:NamedIndividual ;
	tio:time-position "2014-05-28T00:00:00+02:00"^^xsd:dateTime .
}
