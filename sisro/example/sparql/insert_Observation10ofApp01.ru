PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX soo: <http://purl.org/net/sisr/owl/observation#>
PREFIX tio: <http://purl.org/net/sisr/owl/time#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX dedo: <http://purl.org/net/sisr/owl/dedo#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX ado: <http://purl.org/net/sisr/owl/application#>
PREFIX : <http://localhost:5820/sisro/data#>

# Inserting observation relating to an application
# NOTE: it requires a Time instance and an Application
INSERT DATA
{ 
:Observation-10-RAM-App01 rdf:type soo:Observation ; # the URI (i.e. ':Observation-10-RAM-App01') must be unique, it identifies an observation
	rdf:type owl:NamedIndividual ;
	soo:hasObservedProperty ado:memory-usage ;
	soo:hasObservedPropertyUnit ado:memory-usage-unit ;	
	soo:hasProcedure :App01 ;					# an available application must be used here see 'insert_App01.ru'
	soo:result-value "111"^^xsd:string ;
	soo:result-unit "MB"^^xsd:token ;
	soo:hasResultTime :TimeInstant-10 .		# the observed time instance must be used here see 'insert_ObservationTime10.ru'

:Observation-10-Store-App01 rdf:type soo:Observation ; # # the URI (i.e. ':Observation-10-Store-App01') must be unique, it identifies an observation
	rdf:type owl:NamedIndividual ;
	soo:hasObservedProperty ado:storage-usage ;
	soo:hasObservedPropertyUnit ado:storage-usage-unit ;	
	soo:hasProcedure :App01 ;					# an available application must be used here see 'insert_App01.ru'
	soo:result-value "0.111"^^xsd:string ;
	soo:result-unit "GB"^^xsd:token ;
	soo:hasResultTime :TimeInstant-10 .		# the observed time instance must be used here see 'insert_ObservationTime10.ru'
	
:Observation-10-CPU-App01 rdf:type soo:Observation ; # the URI (i.e. ':Observation-10-CPU-App01') must be unique, it identifies an observation
	rdf:type owl:NamedIndividual ;
	soo:hasObservedProperty ado:cpu-computing-power-usage ;
	soo:hasObservedPropertyUnit ado:cpu-computing-power-unit ;	
	soo:hasProcedure :App01 ;					# an available application must be used here see 'insert_App01.ru'
	soo:result-value "5"^^xsd:string ;
	soo:result-unit "%"^^xsd:token ;
	soo:hasResultTime :TimeInstant-10 .		# the observed time instance must be used here see 'insert_ObservationTime10.ru'
}
