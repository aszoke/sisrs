PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX soo: <http://purl.org/net/sisr/owl/observation#>
PREFIX tio: <http://purl.org/net/sisr/owl/time#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX dedo: <http://purl.org/net/sisr/owl/dedo#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX : <http://localhost:5820/sisro/data#>

# Inserting observation relating to a device
# NOTE: it requires a Time instance and a Device
INSERT DATA
{ 
:Observation-11-RAM rdf:type soo:Observation ; # the URI (i.e. ':Observation-11-CPU') must be unique, it identifies an observation
	rdf:type owl:NamedIndividual ;
	soo:hasObservedProperty dedo:operational-memory-capacity-available ;
	soo:hasObservedPropertyUnit dedo:operational-memory-capacity-unit ;	
	soo:hasProcedure :Device01 ;				# an available device must be used here see 'insert_Device01.ru'
	soo:result-value "385"^^xsd:string ;
	soo:result-unit "MB"^^xsd:token ;
	soo:hasResultTime :TimeInstant-11 . 		# the observed time instance must be used here see 'insert_ObservationTime11.ru'

:Observation-11-Store rdf:type soo:Observation ; # the URI (i.e. ':Observation-11-CPU') must be unique, it identifies an observation
	rdf:type owl:NamedIndividual ;
	soo:hasObservedProperty dedo:storage-capacity-available ;
	soo:hasObservedPropertyUnit dedo:storage-capacity-unit ;
	soo:hasProcedure :Device01 ;				# an available device must be used here see 'insert_Device01.ru'
	soo:result-value "2.140"^^xsd:string ;
	soo:result-unit "GB"^^xsd:token ;
	soo:hasResultTime :TimeInstant-11 . 		# the observed time instance must be used here see 'insert_ObservationTime11.ru' 

:Observation-11-CPU rdf:type soo:Observation ; # the URI (i.e. ':Observation-11-CPU') must be unique, it identifies an observation
	rdf:type owl:NamedIndividual ;
	soo:hasObservedProperty dedo:cpu-computing-power-available ;
	soo:hasObservedPropertyUnit dedo:cpu-computing-power-unit ;	
	soo:hasProcedure :Device01 ;				# an available device must be used here see 'insert_Device01.ru'
	soo:result-value "45"^^xsd:string ;
	soo:result-unit "%"^^xsd:token ;
	soo:hasResultTime :TimeInstant-11 . 		# the observed time instance must be used here see 'insert_ObservationTime11.ru'
}
