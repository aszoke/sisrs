# Notes on the sample queries #

The following queries does not include delete operations (due to the complexity of dealing with dependencies i.e. if there are references to the deletable entity).

**Installation of devices and applications**:

* Insering devices and applications (such as `insert_Device01.ru` and `insert_App01.ru`)
* Starting an application on a device (such as `start_App01onDevice01.ru`)
* Stopping an application on a device (such as `stop_App01onDevice01.ru`)

_Note_: an application relocation is made up of two steps: stop and start an application on a device.

**Adding observations to devices and applications**

* Registering a time instance (such as `insert_ObservationTime10.ru`)
* Inserting observations relating to a previously installed device on a registered time instance (such as `insert_Observation10ofDevice01.ru`)
* Inserting observations relating to a previously installed application on a registered time instance (such as `insert_Observation10ofApp01.ru`)

_Note_: For more information, see the comments in the examples.
