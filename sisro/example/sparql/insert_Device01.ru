PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX dedo: <http://purl.org/net/sisr/owl/dedo#>
PREFIX : <http://localhost:5820/sisro/data#>

# Inserting the device into the store
INSERT DATA
{ 
:Device01 rdf:type dedo:Device ;
	rdf:type owl:NamedIndividual ;
	dedo:software-property-ip-address "192.168.0.1"^^xsd:string ;
	dedo:software-property-ssh-port "22"^^xsd:string ;
	dedo:device-type "Android Smartphone"^^xsd:string .

:Device01Hardware rdf:type dedo:Hardware ;
	rdf:type owl:NamedIndividual ;
	dedo:isPartOf :Device01 .

:Device01HardwareCPU rdf:type dedo:CPU ;
	rdf:type dedo:NamedIndividual ;
	dedo:isPartOf :Device01Hardware ;
	dedo:cpu-description "Dual-core 1.2 GHz Cortex-A9"^^xsd:string ;
	dedo:cpu-computing-power "1.1"^^xsd:double ;
	dedo:cpu-computing-power-unit "MIPS"^^xsd:string .

:Device01HardwareMemory-RAM rdf:type dedo:OperationalMemory ;
	rdf:type owl:NamedIndividual ;
	dedo:isPartOf :Device01Hardware ;
	dedo:operational-memory-capacity "2.0"^^xsd:double ;
	dedo:operational-memory-capacity-unit "MB"^^xsd:string .

:Device01HardwareMemory-Store rdf:type dedo:Storage ;
	rdf:type owl:NamedIndividual ;
	dedo:isPartOf :Device01Hardware ;
	dedo:storage-capacity "32.0"^^xsd:double ;
	dedo:storage-capacity-unit "GB"^^xsd:string .
} 