PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX dedo: <http://purl.org/net/sisr/owl/dedo#>
PREFIX ado: <http://purl.org/net/sisr/owl/application#>
PREFIX : <http://localhost:5820/sisro/data#>

# Inserting the application into the store
INSERT DATA
{ 
:App01 rdf:type dedo:Software , dedo:Info , ado:Application ;
	rdf:type owl:NamedIndividual ;
	ado:execution-zip-url "http://192.168.0.200/app01.zip"^^xsd:string ;
	ado:execution-main-exe "main"^^xsd:string ;
	ado:execution-exe-type "war"^^xsd:string ;
	ado:execution-cmd-args "-speed low"^^xsd:string ;
	ado:execution-db-url "jdbc:mysql://192.168.0.100/app01db"^^xsd:string ;
	ado:execution-db-user "johndoe"^^xsd:string ;
	ado:execution-db-password "1234"^^xsd:string ;
	dedo:product-info-name "Runtastic"^^xsd:string ;
	dedo:product-info-vendor "Runtastic Inc."^^xsd:string ;
	dedo:product-info-version "1.0"^^xsd:string .	
}

