PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX dedo: <http://purl.org/net/sisr/owl/dedo#>
PREFIX : <http://localhost:5820/sisro/data#>

# Inserting the device into the store
INSERT DATA
{
:Device02 rdf:type dedo:Device ;
	rdf:type owl:NamedIndividual ;
	dedo:software-property-ip-address "192.168.0.2"^^xsd:string ;
	dedo:software-property-ssh-port "22"^^xsd:string ;	
	dedo:device-type "iPhone Smartphone"^^xsd:string .

:Device02Hardware rdf:type dedo:Hardware ;
	rdf:type owl:NamedIndividual ;
	dedo:isPartOf :Device02 .

:Device02HardwareCPU rdf:type dedo:CPU ;
	rdf:type dedo:NamedIndividual ;
	dedo:isPartOf :Device02Hardware ;
	dedo:cpu-description "Dual-core 1.2 GHz Cortex-A9"^^xsd:string ;
	dedo:cpu-computing-power "1.1"^^xsd:double ;
	dedo:cpu-computing-power-unit "MIPS"^^xsd:string .

:Device02HardwareMemory-RAM rdf:type dedo:OperationalMemory ;
	rdf:type owl:NamedIndividual ;
	dedo:isPartOf :Device02Hardware ;
	dedo:operational-memory-capacity "1.0"^^xsd:double ;
	dedo:operational-memory-capacity-unit "GB"^^xsd:string .

:Device02HardwareMemory-Store rdf:type dedo:Storage ;
	rdf:type owl:NamedIndividual ;
	dedo:isPartOf :Device02Hardware ;
	dedo:storage-capacity "16.0"^^xsd:double ;
	dedo:storage-capacity-unit "GB"^^xsd:string .
}
