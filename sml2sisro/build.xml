<!-- 
	Usage: 
	 - "ant smlvalidate" or 
	 - "ant sml2sisro"
	IMPORTANT NOTE: 
	1) The factory settings below is important: otherwise it leads to classpath loading problem. (SAXON <-> XALAN problem!)
	See: http://stackoverflow.com/questions/919692/how-to-execute-xslt-2-0-with-ant and
	http://stackoverflow.com/questions/5516372/ant-xslt-2-0-with-saxon9-loading-stylesheet-very-very-slow
	2) If the process results in 'build java.lang.OutOfMemoryError: PermGen space' error add the following environmental variable to the system:
	ANT_OPTS=-XX:MaxPermSize=128m
-->
<project name="sensor.web" default="main" basedir=".">
	<property environment="env"/>											<!-- define prefix for environment variables -->
	<property name="sml101_input.dir" location="./src/main/xml/SML_1.0.1" />			<!-- directory of sensor ml input files -->
	<property name="sml20_input.dir" location="./src/main/xml/SML_2.0" />				<!-- directory of sensor ml input files -->
	<property name="sos20_input.dir" location="./src/main/xml/SOS_2.0" />				<!-- directory of sensor observation input files -->
	<property name="temp.dir" location="./temp" />								<!-- directory of temporarly generated files -->
	<property name="log.dir" location="./log" />									<!-- directory of schematron validation result log -->
	<property name="rng.dir" location="./src/main/resource" />						<!-- directory of rng schema file -->

	<property name="sml2sisro.dir" location="./src/main/xsl" />						<!-- directory of sml2sisro transformation -->
	<property name="sos2sisro.dir" location="./src/main/xsl" />						<!-- directory of sos2sisro transformation -->
	<property name="sisro_output.dir" location="./target" />							<!-- directory of sml2sisro transformation output -->

	<property name="res.dir" location="./src/main/resource" />						<!-- directory of resources -->
	<property name="res-schematron.dir" location="./src/main/resource/iso-schematron" />	<!-- directory of schematron validation resources -->
		
	<!-- files -->
	<property name="schematron-schema.file" value="sensorMLProfileForDiscovery" />
	
	<!-- utilities -->
	<property name="saxon" location="./src/main/resource/saxon9/saxon9he.jar" />
	<property name="jing" location="./src/main/resource/jing-20091111/bin/jing.jar" />
	<property name="java" location="${env.JAVA_HOME}/bin"  />
	
	<!-- adding ant-contrib to the ant -->
	<taskdef resource="net/sf/antcontrib/antcontrib.properties">
		<classpath>
			<pathelement location="./src/main/resource/ant-contrib/ant-contrib-1.0b3.jar"/>
		</classpath>
	</taskdef>

	<!-- =============================================
	Misc targets: clean
	============================================== -->

	<!-- ********************************
		Target: clean
	******************************** -->
	<target name="clean" depends="delete-smlvalidate, delete-x2sisro">
		<echo message="::::: The workspace is cleaned. :::::"/>
	</target>
	
	<!-- =============================================
	Validation of sensor ML input : smlvalidate
	============================================== -->
	
	<!-- ********************************
		Target: smlvalidate
	******************************** -->
	<target name="smlvalidate" depends="delete-smlvalidate, makedir-smlvalidate, validating-schematron">
		<echo message="::::: The SensorML validation is started. :::::"/>
		<foreach target="smlvalidate-one" param="file-name">  
			<fileset dir="${sml101_input.dir}" casesensitive="yes">
				<include name="*.xml"/>
			</fileset>
		</foreach>
		<echo message="::::: The SensorML validation is completed. :::::"/>
	</target>
	
	<!-- ********************************
		Target: delete-smlvalidate
	******************************** -->
	<target name="delete-smlvalidate">
		<echo message="Deleting the temporary files of the 'smlvalidate' process..."/>
		<delete dir="${temp.dir}" />
		<delete dir="${log.dir}" />
	</target>

	<!-- ********************************
		Target: makedir-smlvalidate
	******************************** -->
	<target name="makedir-smlvalidate">
		<echo message="Building the workspace for the 'smlvalidate' process"/>
		<mkdir dir="${temp.dir}" />
		<mkdir dir="${log.dir}" />
	</target>

	<!-- ********************************
		Target: validating-schematron
	******************************** -->
	<taskdef name="jing" classname="com.thaiopensource.relaxng.util.JingTask">
			<classpath>
			<pathelement location="${jing}"/>
		</classpath>
	</taskdef>
	
	<target name="validating-schematron">
		<echo message="Validating RELAX NG schema with Jing..."/>
		<!-- validating all the files -->
		<jing rngfile="${res.dir}/iso-schematron.rng">
			<fileset dir="${res.dir}" includes="*.sch"/>
		</jing>
	</target>

	<!-- ********************************
		Target: schemvalidating-one
	******************************** -->
	<target name="smlvalidate-one">
		<echo message="Compiling SCHEMATRON rules to XSLT to validate the input XML document..."/>
		<echo message="expanding inclusions..."/>
		<basename property="basename-wo-suffix" file="${file-name}" suffix=".xml"/>
		<xslt basedir="." style="${res-schematron.dir}/iso_dsdl_include.xsl" in="${res.dir}/${schematron-schema.file}.sch" out="${temp.dir}/${schematron-schema.file}.1.sch">
			<classpath>
				<pathelement location="${saxon}"/>
			</classpath>
			<factory name="net.sf.saxon.TransformerFactoryImpl"/>
		</xslt>
		<echo message="expanding abstract patterns..."/>
		<xslt basedir="." style="${res-schematron.dir}/iso_abstract_expand.xsl" in="${temp.dir}/${schematron-schema.file}.1.sch" out="${temp.dir}/${schematron-schema.file}.2.sch">
			<classpath>
				<pathelement location="${saxon}"/>
			</classpath>
			<factory name="net.sf.saxon.TransformerFactoryImpl"/>
		</xslt>
		<echo message="compiling..."/>
		<xslt basedir="." style="${res-schematron.dir}/iso_svrl_for_xslt2.xsl" in="${temp.dir}/${schematron-schema.file}.2.sch" out="${temp.dir}/${schematron-schema.file}.xsl">
			<classpath>
				<pathelement location="${saxon}"/>
			</classpath>
			<factory name="net.sf.saxon.TransformerFactoryImpl"/>
		</xslt>
		<echo message="validating..."/>
		<xslt basedir="." style="${temp.dir}/${schematron-schema.file}.xsl" in="${sml101_input.dir}/${basename-wo-suffix}.xml" out="${temp.dir}/${basename-wo-suffix}.svrlt">
			<classpath>
				<pathelement location="${saxon}"/>
			</classpath>
			<factory name="net.sf.saxon.TransformerFactoryImpl"/>
		</xslt>
		<echo message="rendering html report..."/>
		<xslt basedir="." style="${res.dir}/svrl2html.xsl" in="${temp.dir}/${basename-wo-suffix}.svrlt" out="${log.dir}/${basename-wo-suffix}.html">
			<classpath>
				<pathelement location="${saxon}"/>
			</classpath>
			<factory name="net.sf.saxon.TransformerFactoryImpl"/>
		</xslt>
	</target>

	<!-- =============================================
	Transform SensorML to SISRO: sml2sisro
	============================================== -->

	<!-- ********************************
		Target: sml2sisro
	******************************** -->
	<target name="sml2sisro" depends="delete-x2sisro, makedir-x2sisro">
		<echo message="::::: The SensorML2SISRO transformation is started. :::::"/>
		<foreach target="sml2sisro-one" param="file-name">  
			<fileset dir="${sml101_input.dir}" casesensitive="yes">
				<include name="*.xml"/>
			</fileset>
		</foreach>
		<echo message="::::: The SensorML2SISRO transformation is completed. :::::"/>
	</target>

	<!-- ********************************
		Target: sml2sisro-one
		TODO: before transformation, validation ('smlvalidate') should be checked!
	******************************** -->
	<target name="sml2sisro-one">
		<echo message="Transforming SensorML input to SISRO ontology..."/>
		<basename property="basename-wo-suffix" file="${file-name}" suffix=".xml"/>
		<echo message="transformation..."/>
		<xslt basedir="." style="${sml2sisro.dir}/sml2sisro.xsl" in="${sml101_input.dir}/${basename-wo-suffix}.xml" out="${sisro_output.dir}/i_${basename-wo-suffix}.rdf">
			<classpath>
				<pathelement location="${saxon}"/>
			</classpath>
			<factory name="net.sf.saxon.TransformerFactoryImpl"/>
		</xslt>
	</target>

	<!-- =============================================
	Transform SOS to SISRO: sos2sisro
	============================================== -->

	<!-- ********************************
		Target: sos2sisro
	******************************** -->
	<target name="sos2sisro" depends="delete-x2sisro, makedir-x2sisro">
		<echo message="::::: The SOS2SISRO transformation is started. :::::"/>
		<foreach target="sos2sisro-one" param="file-name">  
			<fileset dir="${sos20_input.dir}" casesensitive="yes">
				<include name="*.xml"/>
			</fileset>
		</foreach>
		<echo message="::::: The SOS2SISRO transformation is completed. :::::"/>
	</target>

	<!-- ********************************
		Target: sos2sisro-one
	******************************** -->
	<target name="sos2sisro-one">
		<echo message="Transforming SOS input to SISRO ontology..."/>
		<basename property="basename-wo-suffix" file="${file-name}" suffix=".xml"/>
		<echo message="transformation..."/>
		<xslt basedir="." style="${sos2sisro.dir}/sos2sisro.xsl" in="${sos20_input.dir}/${basename-wo-suffix}.xml" out="${sisro_output.dir}/i_${basename-wo-suffix}.rdf">
			<classpath>
				<pathelement location="${saxon}"/>
			</classpath>
			<factory name="net.sf.saxon.TransformerFactoryImpl"/>
		</xslt>
	</target>

	<!-- ********************************
		Target: delete-x2sisro
	******************************** -->
	<target name="delete-x2sisro">
		<echo message="Deleting the temporary files of the transformation process..."/>
		<delete dir="${sisro_output.dir}" />
	</target>

	<!-- ********************************
		Target: makedir-x2sisro
	******************************** -->
	<target name="makedir-x2sisro">
		<echo message="Building the workspace for the transformation process..."/>
		<mkdir dir="${sisro_output.dir}" />
	</target>

</project>
