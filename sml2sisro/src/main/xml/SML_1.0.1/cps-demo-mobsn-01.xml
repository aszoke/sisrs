<?xml version="1.0" encoding="UTF-8"?>
<!-- ****************************************************************
This section explains the SensorML profile for sensor and SWE service discovery basedon a SensorML 
example that formally describes a smart phone.
**************************************************************** -->
<SensorML xmlns="http://www.opengis.net/sensorML/1.0.1" xmlns:swe="http://www.opengis.net/swe/1.0.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:gml="http://www.opengis.net/gml" xsi:schemaLocation="http://www.opengis.net/sensorML/1.0.1 http://schemas.opengis.net/sensorML/1.0.1/sensorML.xsd" version="1.0.1">
	<member>
		<!-- ****************************************************************
	The smart phone can be considered as a platform for several detectors. These detector components are 
	aggregated by the phone.
	**************************************************************** -->
		<System>
			<gml:description>A system which includes six sensors (compass, proximity, ambient light, gyroscope, accelerometer, barometer).</gml:description>
			<!-- ****************************************************************
	  The  keywords section provides a list of keywords which may be used by a user to search for 
	  a sensor. The  keywords section MUST be provided by any  System .
	  **************************************************************** -->
			<keywords>
				<KeywordList>
					<keyword>compass</keyword>
					<keyword>proximity</keyword>
					<keyword>ambient light</keyword>
					<keyword>gyroscope</keyword>
					<keyword>accelerometer</keyword>
					<keyword>barometer</keyword>
				</KeywordList>
			</keywords>
			<!-- ****************************************************************      
	  The  identification section informs about general attributes which identify the  system among others.  
	  This profile requires that at least the following elements MUST be present for any  System: 
	  uniqueID, longName, shortName
	  **************************************************************** -->
			<identification>
				<IdentifierList>
					<identifier name="uniqueID">
						<Term definition="urn:ogc:def:identifier:OGC:1:0:uniqueID">
							<value>http://localhost:5820/sisro/System/CPS-Demo-MOBSN-01</value>
						</Term>
					</identifier>
					<identifier name="longName">
						<Term definition="urn:ogc:def:identifier:OGC:1.0:longName">
							<value>CPS Demo MOBSN-01 System on the third floor E wing of the BME I building</value>
						</Term>
					</identifier>
					<identifier name="shortName">
						<Term definition="urn:ogc:def:identifier:OGC:1.0:shortName">
							<value>CPS Demo MOBSN-01 System</value>
						</Term>
					</identifier>
				</IdentifierList>
			</identification>
			<!-- ****************************************************************      
	  The classification section gives further information regarding the type of the System.      
	  This information MAY include the intended application of the  System or the types of the affiliated 
	  sensors.
	  **************************************************************** -->
			<classification>
				<ClassifierList>
					<classifier name="intendedApplication">
						<Term definition="urn:ogc:def:classifier:OGC:1.0:application">
							<value>mobile applications</value>
						</Term>
					</classifier>
				</ClassifierList>
			</classification>
			<!-- ****************************************************************      
	  The  validTime section provides information about the time period in which a sensor description 
	  is valid. Each  System MUST contain this information.      
	  **************************************************************** -->
			<validTime>
				<gml:TimePeriod>
					<gml:beginPosition>2014-03-30</gml:beginPosition>
					<gml:endPosition>2016-03-30</gml:endPosition>
				</gml:TimePeriod>
			</validTime>
			<!-- ****************************************************************      
	  The capabilities element MAY be used to specify attributes which capture configuration of the 
	  present station instance or describe the current status. 
	  **************************************************************** -->
			<capabilities>
				<!-- ****************************************************************        
		A swe:DataRecord containing a number of  swe:field elements MUST be used here to specify 
		the capabilities of the  System or  Component. if the child-element of the  swe:Field is a swe:Quantity 
		it has to contain the  swe:uom element which specifies the  code attribute. one  swe:field MUST 
		contain a  swe:Envelope element with the definition urn:ogc:def:property:OGC:1.0:observedBBOX . 
		It describes the bounding box of the area that is observed by the System. In case of an in-situ 
		sensor this bounding box only contains the position of the sensor.
        **************************************************************** -->
				<swe:DataRecord definition="urn:ogc:def:property:capabilities">
					<swe:field name="status">
						<swe:Text definition="urn:ogc:def:property:OGC:1.0:status">
							<gml:description>The operating status of the system.</gml:description>
							<swe:value>active</swe:value>
						</swe:Text>
					</swe:field>
					<swe:field name="observedBBOX">
						<swe:Envelope definition="urn:ogc:def:property:OGC:1.0:observedBBOX">
							<swe:lowerCorner>
								<swe:Vector>
									<swe:coordinate name="easting">
										<swe:Quantity axisID="x">
											<swe:uom code="DD"/>
											<swe:value>19.060233</swe:value>
										</swe:Quantity>
									</swe:coordinate>
									<swe:coordinate name="northing">
										<swe:Quantity axisID="y">
											<swe:uom code="DD"/>
											<swe:value>47.472563</swe:value>
										</swe:Quantity>
									</swe:coordinate>
								</swe:Vector>
							</swe:lowerCorner>
							<swe:upperCorner>
								<swe:Vector>
									<swe:coordinate name="easting">
										<swe:Quantity axisID="x">
											<swe:uom code="DD"/>
											<swe:value>19.060249</swe:value>
										</swe:Quantity>
									</swe:coordinate>
									<swe:coordinate name="northing">
										<swe:Quantity axisID="y">
											<swe:uom code="DD"/>
											<swe:value>47.472505</swe:value>
										</swe:Quantity>
									</swe:coordinate>
								</swe:Vector>
							</swe:upperCorner>
						</swe:Envelope>
					</swe:field>
				</swe:DataRecord>
			</capabilities>
			<!-- ****************************************************************      
	  The contact section references the organization or person which is responsible for the sensor. 
	  This information MUST be provided for every  System. In general this is the provider of the sensor. 
	  **************************************************************** -->
			<contact>
				<ResponsibleParty gml:id="CPS_BME_contact">
					<organizationName>Budapest University of Techonology, MIT Department, CPS Project</organizationName>
					<contactInfo>
						<address>
							<electronicMailAddress>cps-project@mit.bme.hu</electronicMailAddress>
						</address>
					</contactInfo>
				</ResponsibleParty>
			</contact>
			<!-- ****************************************************************      
	  The position section specifies the sensor’s position in space. It MUST be given for every System. 
	  **************************************************************** -->
			<position name="stationPosition">
				<swe:Position referenceFrame="urn:ogc:def:crs:EPSG:6.14:31466">
					<swe:location>
						<swe:Vector gml:id="SYSTEM_LOCATION">
							<swe:coordinate name="longitude">
								<swe:Quantity axisID="x">
									<swe:uom code="DD"/>
									<swe:value>19.060225</swe:value>
								</swe:Quantity>
							</swe:coordinate>
							<swe:coordinate name="latitude">
								<swe:Quantity axisID="y">
									<swe:uom code="DD"/>
									<swe:value>47.472556</swe:value>
								</swe:Quantity>
							</swe:coordinate>
							<swe:coordinate name="altitude">
								<swe:Quantity axisID="z">
									<swe:uom code="m"/>
									<swe:value>12.0</swe:value>
								</swe:Quantity>
							</swe:coordinate>
						</swe:Vector>
					</swe:location>
				</swe:Position>
			</position>
			<!-- ****************************************************************      
	  The  inputs section is a MUST for every  System. It lists up the phenomena observed by the 
	  different sensors of the system. Therefore every input utilizes the swe:ObservableProperty and 
	  its  definition attribute which takes a URN to point to a dictionary entry.  This dictionary entry 
	  defines the measured phenomenon in detail and declares its semantics.      
	  **************************************************************** -->
			<inputs>
				<InputList>
					<input name="magnetization">
						<swe:ObservableProperty definition="urn:ogc:def:property:CPS:1.0:magnetization"/>
						<!-- for Compass -->
					</input>
					<input name="electromagneticField">
						<swe:ObservableProperty definition="urn:ogc:def:property:CPS:1.0:electromagneticField"/>
						<!-- for Proximity -->
					</input>
					<input name="light">
						<swe:ObservableProperty definition="urn:ogc:def:property:CPS:1.0:light"/>
						<!-- for Ambient light -->
					</input>
					<input name="orientation">
						<swe:ObservableProperty definition="urn:ogc:def:property:CPS:1.0:orientation"/>
						<!-- for Gyroscope -->
					</input>
					<input name="acceleration">
						<swe:ObservableProperty definition="urn:ogc:def:property:CPS:1.0:acceleration"/>
						<!-- for Accelerometer -->
					</input>
					<input name="atmosphericPressure">
						<swe:ObservableProperty definition="urn:ogc:def:property:CPS:1.0:atmosphericPressure"/>
						<!-- for Barometer -->
					</input>
				</InputList>
			</inputs>
			<!-- ****************************************************************      
	  Within the  outputs section which is a MUST for every System, the  signals recorded by the 
	  sensors are defined. The sub-element of each output references the captured phenomenon 
	  and specifies the unit of measure in which the measured values are expressed.      
	  **************************************************************** -->
			<outputs>
				<OutputList>
					<output name="magnetization">
						<swe:Quantity definition="urn:ogc:def:property:CPS:1.0:magnetization">
							<!-- for Compass -->
							<swe:uom code="B"/>
						</swe:Quantity>
					</output>
					<output name="electromagneticField">
						<swe:Quantity definition="urn:ogc:def:property:CPS:1.0:electromagneticField">
							<!-- for Proximity -->
							<swe:uom code="mm"/>
						</swe:Quantity>
					</output>
					<output name="light">
						<swe:Quantity definition="urn:ogc:def:property:CPS:1.0:light">
							<!-- for Ambient light -->
							<swe:uom code="lumen"/>
						</swe:Quantity>
					</output>
					<output name="orientation">
						<swe:Quantity definition="urn:ogc:def:property:CPS:1.0:orientation">
							<!-- for Gyroscope -->
							<swe:uom code="orientation_unit"/>
						</swe:Quantity>
					</output>
					<output name="acceleration">
						<swe:Quantity definition="urn:ogc:def:property:CPS:1.0:acceleration">
							<!-- for Accelerometer -->
							<swe:uom code="m/s2"/>
						</swe:Quantity>
					</output>
					<output name="atmosphericPressure">
						<swe:Quantity definition="urn:ogc:def:property:CPS:1.0:atmosphericPressure">
							<!-- for Barometer -->
							<swe:uom code="Pa"/>
						</swe:Quantity>
					</output>
				</OutputList>
			</outputs>
		</System>
	</member>
</SensorML>
