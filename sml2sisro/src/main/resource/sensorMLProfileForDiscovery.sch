<?xml version="1.0" encoding="utf-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron" xmlns:iso="http://purl.oclc.org/dsdl/schematron" xmlns:dp="http://www.dpawson.co.uk/ns#" queryBinding='xslt2' schemaVersion='ISO19757-3'>
	<title>Schematron file for SensorML profile for discovery.</title>
	<ns prefix='sml' uri='http://www.opengis.net/sensorML/1.0.1'/>
	<ns prefix='swe' uri='http://www.opengis.net/swe/1.0.1'/>
	<ns prefix='xlink' uri='http://www.w3.org/1999/xlink'/>
	<!-- **********************************************************************
	Common Profile Rules
	********************************************************************** -->
	<pattern id="common.profile.rules">
		<!-- Schematron rule for ensuring that a KeywordList is provided -->
		<rule context="//sml:System">
			<assert test="sml:keywords/sml:KeywordList">Error: 'KeywordList' element has to be present</assert>
			<report test="count(sml:keywords/sml:KeywordList)"><value-of select="count(sml:keywords/sml:KeywordList)" /> keyword lists</report>
		</rule>
		<rule context="//sml:Component">
			<assert test="sml:keywords/sml:KeywordList">Error: 'KeywordList' element has to be present</assert>
		</rule>
		<!-- Schematron rule for ensuring that every identifier contains a definition -->
		<rule context="//sml:identification/sml:IdentifierList/sml:identifier/sml:Term">
			<assert test="string-length(@definition) > 0">Error: 'definition' attribute has to be present and its value has to be > 0.</assert>
		</rule>
		<!-- Schematron rule for ensuring that every classification element contains a definition -->
		<rule context="//sml:classification/sml:ClassifierList/sml:classifier/sml:Term">
			<assert test="string-length(@definition) > 0">Error: 'definition' attribute has to be present and its value has to be > 0.</assert>
		</rule>
		<!-- Schematron rule for ensuring that a uniqueID is provided -->
		<rule context="//sml:identification">
			<assert test="count(sml:IdentifierList/sml:identifier/sml:Term[@definition = 'urn:ogc:def:identifier:OGC:1:0:uniqueID']) = 1">Error: one identifier has to be of the type 'urn:ogc:def:identifier:OGC:1:0:uniqueID'.</assert>
		</rule>
		<!-- Schematron rule for ensuring that a longName and a shortName are provided -->
		<rule context="//sml:identification">
			<assert test="count(sml:IdentifierList/sml:identifier/sml:Term[@definition = 'urn:ogc:def:identifier:OGC:1:0:longName']) = 1">Error: one identifier has to be of the type 'urn:ogc:def:identifier:OGC:1:0:longName'.</assert>
		</rule>
		<rule context="//sml:identification">
			<assert test="count(sml:IdentifierList/sml:identifier/sml:Term[@definition = 'urn:ogc:def:identifier:OGC:1:0:shortName']) = 1">Error: one identifier has to be of the type 'urn:ogc:def:identifier:OGC:1:0:shortName'.</assert>
		</rule>
		<!-- Schematron rule for restricting the structure of the capabilities section -->
		<rule context="//sml:capabilities/swe:DataRecord/swe:field">
			<assert test="string-length(child::node()[@definition]) > 0">Error: 'definition' attribute has to be present and its value has to be > 0.</assert>
		</rule>
		<!-- Schematron rule for ensuring that the unit of measurement is provided if in the capabilities section a swe:Quantity is used -->
		<rule context="//sml:capabilities/swe:DataRecord/swe:field/swe:Quantity/swe:uom">
			<assert test="string-length(@code) > 0">Error: 'code' attribute has to be present and its value has to be > 0.</assert>
		</rule>
		<!-- Schematron rule for ensuring that an inputs and an outputs section are provided -->
		<rule context="//sml:System">
			<assert test="sml:inputs">Error: 'sml:inputs' has to be present.</assert>
		</rule>
		<rule context="//sml:Component">
			<assert test="sml:inputs">Error: 'sml:inputs' has to be present.</assert>
		</rule>
		<rule context="//sml:System">
			<assert test="sml:outputs">Error: 'sml:outputs' has to be present.</assert>
		</rule>
		<rule context="//sml:Component">
			<assert test="sml:outputs">Error: 'sml:outputs' has to be present.</assert>
		</rule>
		<!--  Schematron rule for ensuring that the inputs and outputs sections contain a definition element -->
		<rule context="//sml:inputs/sml:InputList/sml:input">
			<assert test="swe:ObservableProperty/@definition">Error!</assert>
		</rule>
		<rule context="//sml:outputs/sml:OutputList/sml:output">
			<assert test="child::node()[@definition]">Error!</assert>
		</rule>
		</pattern>
		<!-- **********************************************************************
		System Specific Profile Rules
		********************************************************************** -->
		<pattern id="system.specific.profile.rules">
		<!-- Schematron rule for ensuring that a member element contains exactly one system -->
		<rule context="/">
			<assert test="count(sml:SensorML/sml:member) = 1">Error!</assert>
			<assert test="count(sml:SensorML/sml:member/sml:System) = 1">Error!</assert>
		</rule>
		<!-- Schematron rule for ensuring that for every System a validTime element is provided -->
		<rule context="//sml:System">
			<assert test="sml:validTime">Error: 'validTime' element has to be present</assert>
		</rule>
		<!-- Schematron rule for ensuring that an observedBBOX is provided -->
		<rule context="//sml:System/sml:capabilities">
			<assert test="count(swe:DataRecord/swe:field/swe:Envelope[@definition = 'urn:ogc:def:property:OGC:1.0:observedBBOX']) = 1">Error: one "swe:field" of the "DataRecord" has to contain a "swe:Envelope" element with the definition "urn:ogc:def:property:OGC:1.0:observedBBOX".</assert>
		</rule>
		<!-- Schematron rule for ensuring that for every System a contact element is provided -->
		<rule context="//sml:System">
			<assert test="sml:contact">Error: 'sml:contact"' element has to be present</assert>
		</rule>
		<!-- Schematron rule for ensuring that a Position (including a referenceFrame attribute) is provided -->
		<rule context="//sml:position/swe:Position">
			<assert test="@referenceFrame">Error!</assert>
		</rule>
		<!-- Schematron rule for defining the description of the position -->
		<rule context="//sml:position/swe:Position/swe:location">
			<assert test="count(swe:Vector/swe:coordinate/swe:Quantity) > 1">Error!</assert>
		</rule>
		<!-- Schematron rule for defining the format of coordinates -->
		<rule context="//sml:position/swe:Position/swe:location/swe:Vector/swe:coordinate/swe:Quantity">
			<assert test="string-length(@axisID) > 0">Error!</assert>
			<assert test="swe:value">Error!</assert>
			<assert test="swe:uom[@code]">Error!</assert>
		</rule>
		<!--  Schematron rule for ensuring that descriptions for the Components of a System are provided -->
		<rule context="//sml:System/sml:components/sml:ComponentList/sml:component">
			<assert test="(@xlink:href and not(sml:Component)) or (not (@xlink:href) and sml:Component)">Error!</assert>
		</rule>
		</pattern>
		<!-- **********************************************************************
		Component Specific Profile Rules
		********************************************************************** -->
		<pattern id="component.specific.profile.rules">
		<!-- Schematron rule for ensuring that for every Component the sensor type is described -->
		<rule context="//sml:Component/sml:classification">
			<assert test="count(sml:ClassifierList/sml:classifier/sml:Term[@definition = 'urn:ogc:def:classifier:OGC:1.0:sensorType']) >= 1">Error!</assert>
		</rule>
	</pattern>
</schema>
