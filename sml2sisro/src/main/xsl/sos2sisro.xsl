<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE RDF [
	<!ENTITY % common SYSTEM "common.dtd">
	<!ENTITY % instance SYSTEM "instance.dtd">
	%common;
	%instance;
]>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:gml32="&gml32;" xmlns:owl="&owl;" xmlns:rdf="&rdf;" xmlns:dc="&dc;" xmlns:xsd="&xsd;" xmlns:uuid="http://www.uuid.org" xmlns:xlink="&xlink;" xmlns:sos="&sos;" xmlns:soo="&soo;" xmlns:om="&om;" xmlns:tio="&tio;">
	<!-- **************************************************************************************************** -->
	<!-- Definitions -->
	<!-- **************************************************************************************************** -->
	<xsl:include href="common.xsl"/>
	<xsl:include href="uuid.xslt"/>
	<xsl:output method="xml" version="1.0" encoding="UTF-8" omit-xml-declaration="no" indent="yes"/>
	<!-- **************************************************************************************************** -->
	<!-- Function definitions -->
	<!-- **************************************************************************************************** -->
	<!-- **************************************************************************************************** -->
	<!-- Template definitions -->
	<!-- **************************************************************************************************** -->
	<!-- ********************************************** -->
	<!-- sos:GetObservationResponse -->
	<!-- ********************************************** -->
	<xsl:template match="//sos:GetObservationResponse">
		<xsl:element name="rdf:RDF" namespace="&rdf;">
			<xsl:namespace name="dc" select="'&dc;'"/>
			<xsl:namespace name="owl" select="'&owl;'"/>
			<xsl:namespace name="rdf" select="'&rdf;'"/>
			<xsl:namespace name="xsd" select="'&xsd;'"/>
			<xsl:namespace name="soo" select="'&soo;'"/>
			<xsl:namespace name="tio" select="'&tio;'"/>
			<xsl:namespace name="gml32" select="'&gml32;'"/>
			<xsl:namespace name="xlink" select="'&xlink;'"/>
			<xsl:namespace name="om" select="'&om;'"/>
			<xsl:element name="owl:Ontology">
				<xsl:namespace name="rdf" select="'&rdf;'"/>
				<xsl:attribute name="rdf:about"><xsl:value-of select="concat('&urlprefix;','Observation','#',uuid:get-uuid())"/></xsl:attribute>
				<xsl:element name="dc:creator">SOSML2SISRO</xsl:element>
				<xsl:element name="dc:title">Observation package</xsl:element>
				<xsl:element name="dc:date">
					<xsl:value-of select="current-date()"/>
				</xsl:element>
				<xsl:element name="owl:imports">
					<xsl:attribute name="rdf:resource">&sisro;</xsl:attribute>
				</xsl:element>
			</xsl:element>
			<xsl:for-each select="sos:observationData">
				<xsl:apply-templates select="om:OM_Observation"/>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>
	<!-- ********************************************** -->
	<!-- om:OM_Observation -->
	<!-- ********************************************** -->
	<xsl:template match="om:OM_Observation">
	<!-- Observation time -->
		<xsl:element name="owl:NamedIndividual">
			<xsl:attribute name="rdf:about"><xsl:value-of select="om:phenomenonTime/gml32:TimeInstant/@gml32:id"/></xsl:attribute>
			<xsl:element name="rdf:type">
				<xsl:attribute name="rdf:resource">&tio;TimeInstant</xsl:attribute>
			</xsl:element>
			<xsl:element name="tio:time-position">
				<xsl:attribute name="rdf:datatype">&xsd;dateTime</xsl:attribute>
				<xsl:value-of select="om:phenomenonTime/gml32:TimeInstant/gml32:timePosition"/>
			</xsl:element>
		</xsl:element>
		<!-- Observation data -->
		<xsl:element name="owl:NamedIndividual">
			<xsl:attribute name="rdf:about"><xsl:value-of select="./@gml32:id"/></xsl:attribute>
			<xsl:element name="rdf:type">
				<xsl:attribute name="rdf:resource">&soo;Observation</xsl:attribute>
			</xsl:element>
			<xsl:element name="soo:result-value">
				<xsl:attribute name="rdf:datatype">&xsd;string</xsl:attribute>
				<xsl:value-of select="om:result"/>
			</xsl:element>
			<xsl:element name="soo:result-unit">
				<xsl:attribute name="rdf:datatype">&xsd;token</xsl:attribute>
				<xsl:value-of select="om:result/@uom"/>
			</xsl:element>
			<xsl:element name="soo:hasObservedProperty">
				<xsl:attribute name="rdf:resource"><xsl:value-of select="om:observedProperty/@xlink:href"/></xsl:attribute>
			</xsl:element>
			<xsl:element name="soo:hasProcedure">
				<xsl:attribute name="rdf:resource"><xsl:value-of select="om:procedure/@xlink:href"/></xsl:attribute>
			</xsl:element>
			<xsl:element name="soo:hasResultTime">
				<xsl:attribute name="rdf:resource"><xsl:value-of select="om:resultTime/@xlink:href"/></xsl:attribute>
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
