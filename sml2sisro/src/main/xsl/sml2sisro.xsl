<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE RDF [
  <!ENTITY % common SYSTEM "common.dtd">
  <!ENTITY % instance SYSTEM "instance.dtd">
  %common;
  %instance;
]>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:sml="http://www.opengis.net/sensorML/1.0.1" xmlns:gml="&gml;" xmlns:owl="&owl;" xmlns:rdf="&rdf;" xmlns:dc="&dc;" xmlns:rdfs="&rdfs;" xmlns:keo="&keo;" xmlns:ido="&ido;" xmlns:clo="&clo;" xmlns:DUL="&DUL;" xmlns:sisro="&sisro;" xmlns:cio="&cio;" xmlns:ssn="&ssn;" xmlns:swe="&swe;" xmlns:pro="&pro;" xmlns:geo="&geo;" xmlns:cao="&cao;" xmlns:xsd="&xsd;" xmlns:xml="http://www.w3.org/XML/1998/namespace" xmlns:dedo="&dedo;" xmlns:uuid="http://www.uuid.org" xmlns:xlink="&xlink;">
  <!-- **************************************************************************************************** -->
  <!-- Definitions -->
  <!-- **************************************************************************************************** -->
  <xsl:include href="common.xsl"/>
  <xsl:include href="uuid.xslt"/>
  <xsl:output method="xml" version="1.0" encoding="UTF-8" omit-xml-declaration="no" indent="yes"/>
  <!-- **************************************************************************************************** -->
  <!-- Function definitions -->
  <!-- **************************************************************************************************** -->
  <!-- **************************************************************************************************** -->
  <!-- Template definitions -->
  <!-- **************************************************************************************************** -->
  <!-- ********************************************** -->
  <!-- sml:SensorML -->
  <!-- ********************************************** -->
  <xsl:template match="//sml:SensorML">
    <xsl:element name="rdf:RDF" namespace="&rdf;">
      <xsl:namespace name="dc" select="'&dc;'"/>
      <xsl:namespace name="owl" select="'&owl;'"/>
      <xsl:namespace name="rdf" select="'&rdf;'"/>
      <xsl:namespace name="xml" select="'&xml;'"/>
      <xsl:namespace name="xsd" select="'&xsd;'"/>
      <xsl:namespace name="rdfs" select="'&rdfs;'"/>
      <xsl:namespace name="geo" select="'&geo;'"/>
      <xsl:namespace name="cio" select="'&cio;'"/>
      <xsl:namespace name="ido" select="'&ido;'"/>
      <xsl:namespace name="keo" select="'&keo;'"/>
      <xsl:namespace name="clo" select="'&clo;'"/>
      <xsl:namespace name="pro" select="'&pro;'"/>
      <xsl:namespace name="cao" select="'&cao;'"/>
      <xsl:namespace name="dedo" select="'&dedo;'"/>
      <xsl:namespace name="soo" select="'&soo;'"/>
      <xsl:namespace name="tio" select="'&tio;'"/>      
      <xsl:namespace name="ado" select="'&ado;'"/>
      <xsl:namespace name="sisro" select="'&sisro;'"/>
      <xsl:namespace name="ssn" select="'&ssn;'"/>
      <xsl:namespace name="DUL" select="'&DUL;'"/>
      <xsl:namespace name="gml" select="'&gml;'"/>
      <xsl:namespace name="swe" select="'&swe;'"/>
      <xsl:namespace name="xlink" select="'&xlink;'"/>
      <xsl:element name="owl:Ontology">
        <xsl:namespace name="rdf" select="'&rdf;'"/>
        <xsl:attribute name="rdf:about"><xsl:value-of select="concat(sml:member/sml:System/sml:identification/sml:IdentifierList/sml:identifier[@name='uniqueID']/sml:Term/sml:value,'#')"/></xsl:attribute>
        <xsl:element name="dc:creator">SensorML2SISRO</xsl:element>
        <xsl:element name="dc:title">
          <xsl:value-of select="sml:member/sml:System/gml:description"/>
        </xsl:element>
        <xsl:element name="dc:date">
          <xsl:value-of select="current-date()"/>
        </xsl:element>
        <xsl:element name="owl:imports">
          <!-- Trailing '#' made OWL API cry -->
          <xsl:attribute name="rdf:resource">http://purl.oclc.org/NET/ssnx/ssn</xsl:attribute>
        </xsl:element>
        <xsl:element name="owl:imports">
          <!-- Trailing '#' made OWL API cry -->
          <xsl:attribute name="rdf:resource">http://purl.org/net/sisr/owl/sisro</xsl:attribute>
        </xsl:element>
        <!--
	    <xsl:element name="owl:imports">
          <xsl:attribute name="rdf:resource"><xsl:value-of select="concat(sml:member/sml:System/sml:identification/sml:IdentifierList/sml:identifier[@name='uniqueID']/sml:Term/sml:value,'_DD')"/></xsl:attribute>
		</xsl:element>
	<xsl:element name="owl:imports">
          <xsl:attribute name="rdf:resource"><xsl:value-of select="concat(sml:member/sml:System/sml:identification/sml:IdentifierList/sml:identifier[@name='uniqueID']/sml:Term/sml:value,'_OD')"/></xsl:attribute>
        </xsl:element>
	<xsl:element name="owl:imports">
          <xsl:attribute name="rdf:resource"><xsl:value-of select="concat(sml:member/sml:System/sml:identification/sml:IdentifierList/sml:identifier[@name='uniqueID']/sml:Term/sml:value,'_AD')"/></xsl:attribute>
        </xsl:element>-->
</xsl:element> 
      <xsl:apply-templates select="sml:member"/>
    </xsl:element>
  </xsl:template>
  <!-- ********************************************** -->
  <!-- sml:member -->
  <!-- ********************************************** -->
  <xsl:template match="sml:member">
    <xsl:apply-templates select="sml:System"/>
  </xsl:template>
  <!-- ********************************************** -->
  <!-- sml:System -->
  <!-- ********************************************** -->
  <xsl:template match="sml:System">
    <xsl:call-template name="Component">
      <xsl:with-param name="System" select="//sml:System/sml:identification/sml:IdentifierList/sml:identifier[@name='uniqueID']/sml:Term/sml:value"/>
    </xsl:call-template>
  </xsl:template>
  <!-- ======================================================================== -->
  <!-- ============================== MAIN template ============================== -->
  <!-- ======================================================================== -->
  <!-- ********************************************** -->
  <!-- Component (RECURSIVE template) -->
  <!-- ********************************************** -->
  <xsl:template name="Component">
    <!-- ########################### -->
    <!-- ##### UUID variables for parts ## -->
    <!-- ########################### -->
    <xsl:param name="System"/>
    <xsl:variable name="Contact" select="concat('&urlprefix;','Contact','#',uuid:get-uuid())"/>
    <xsl:variable name="ValidTimeStart" select="concat('&urlprefix;','ValidTimeStart','#',uuid:get-uuid())"/>
    <xsl:variable name="ValidTimeEnd" select="concat('&urlprefix;','ValidTimeEnd','#',uuid:get-uuid())"/>
    <xsl:variable name="Keyword" select="concat('&urlprefix;','Keyword','#',uuid:get-uuid())"/>
    <xsl:variable name="IdUnique" select="concat('&urlprefix;','IdUnique','#',uuid:get-uuid())"/>
    <xsl:variable name="IdLongName" select="concat('&urlprefix;','IdLongName','#',uuid:get-uuid())"/>
    <xsl:variable name="IdShortName" select="concat('&urlprefix;','IdShortName','#',uuid:get-uuid())"/>
    <xsl:variable name="MeasurementCapability" select="concat('&urlprefix;','MeasurementCapability','#',uuid:get-uuid())"/>
    <xsl:variable name="Deployment" select="concat('&urlprefix;','Deployment','#',uuid:get-uuid())"/>
    <xsl:variable name="Classification" select="concat('&urlprefix;','Classification','#',uuid:get-uuid())"/>
    <xsl:variable name="Process" select="concat('&urlprefix;','Process','#',uuid:get-uuid())"/>
    <xsl:variable name="Component" select="concat('&urlprefix;','Component','#',uuid:get-uuid())"/>
    <!-- ########################### -->
    <!-- ##### References to parts ##### -->
    <!-- ########################### -->
    <xsl:element name="owl:NamedIndividual">
      <xsl:namespace name="rdf" select="'&rdf;'"/>
      <xsl:attribute name="rdf:about"><xsl:value-of select="$System"/></xsl:attribute>
      <xsl:element name="rdf:type">
        <xsl:attribute name="rdf:resource">&ssn;System</xsl:attribute>
      </xsl:element>
      <xsl:choose>
        <xsl:when test="name() = 'Component'">
          <xsl:element name="rdfs:label"><xsl:value-of select="../@name"/></xsl:element>
        </xsl:when>
        <xsl:when test="name() = 'System'">
          <xsl:element name="rdfs:label">System</xsl:element>
        </xsl:when>
      </xsl:choose>
      <!-- ##### gml:description -->
      <xsl:if test="gml:description">
        <xsl:element name="rdfs:comment"><xsl:value-of select="gml:description"/></xsl:element>
      </xsl:if>
      <!-- ##### cio:hasContact -->
      <xsl:if test="sml:contact">
        <xsl:element name="cio:hasContact">
          <xsl:attribute name="rdf:resource"><xsl:value-of select="$Contact"/></xsl:attribute>
        </xsl:element>
      </xsl:if>
      <!-- ##### ssn:startTime, ssn:endTime -->
      <xsl:if test="sml:validTime">
        <xsl:element name="ssn:startTime">
          <xsl:attribute name="rdf:resource"><xsl:value-of select="$ValidTimeStart"/></xsl:attribute>
        </xsl:element>
        <xsl:element name="ssn:endTime">
          <xsl:attribute name="rdf:resource"><xsl:value-of select="$ValidTimeEnd"/></xsl:attribute>
        </xsl:element>
      </xsl:if>
      <!-- ##### identification -->
      <xsl:for-each select="sml:identification/sml:IdentifierList/sml:identifier">
        <xsl:choose>
          <xsl:when test="@name = 'uniqueID'">
            <xsl:element name="ido:hasIdentifier">
              <xsl:attribute name="rdf:resource"><xsl:value-of select="$IdUnique"/></xsl:attribute>
            </xsl:element>
          </xsl:when>
          <xsl:when test="@name = 'longName'">
            <xsl:element name="ido:hasIdentifier">
              <xsl:attribute name="rdf:resource"><xsl:value-of select="$IdLongName"/></xsl:attribute>
            </xsl:element>
          </xsl:when>
          <xsl:when test="@name = 'shortName'">
            <xsl:element name="ido:hasIdentifier">
              <xsl:attribute name="rdf:resource"><xsl:value-of select="$IdShortName"/></xsl:attribute>
            </xsl:element>
          </xsl:when>
        </xsl:choose>
      </xsl:for-each>
      <!-- ##### keo:hasKeyword -->
      <xsl:if test="sml:keywords">
        <xsl:element name="keo:hasKeyword">
          <xsl:attribute name="rdf:resource"><xsl:value-of select="$Keyword"/></xsl:attribute>
        </xsl:element>
      </xsl:if>
      <!-- ##### ssn:hasMeasurementCapability -->
      <xsl:for-each select="sml:capabilities/swe:DataRecord/swe:field">
        <xsl:element name="ssn:hasMeasurementCapability">
          <xsl:attribute name="rdf:resource"><xsl:value-of select="concat($MeasurementCapability,position())"/></xsl:attribute>
        </xsl:element>
      </xsl:for-each>
      <!-- ##### ssn:hasDeployment -->
      <xsl:if test="sml:position">
        <xsl:element name="ssn:hasDeployment">
          <xsl:attribute name="rdf:resource"><xsl:value-of select="$Deployment"/></xsl:attribute>
        </xsl:element>
      </xsl:if>
      <!-- ##### clo:hasClassification -->
      <xsl:for-each select="sml:classification/sml:ClassifierList/sml:classifier">
        <xsl:element name="clo:hasClassifier">
          <xsl:attribute name="rdf:resource"><xsl:value-of select="concat($Classification,position())"/></xsl:attribute>
        </xsl:element>
      </xsl:for-each>
      <!-- ##### ssn:implements -->
      <xsl:if test="sml:inputs/sml:InputList/sml:input or sml:outputs/smlOutputList/sml:output">
        <xsl:element name="ssn:implements">
          <xsl:attribute name="rdf:resource"><xsl:value-of select="$Process"/></xsl:attribute>
        </xsl:element>
      </xsl:if>
      <!-- ##### ssn:hasSubSystem -->
      <xsl:for-each select="sml:components/sml:ComponentList/sml:component">
        <xsl:element name="ssn:hasSubSystem">
          <xsl:attribute name="rdf:resource"><xsl:value-of select="concat($Component,position())"/></xsl:attribute>
        </xsl:element>
      </xsl:for-each>
    </xsl:element>
    <!-- ########################### -->
    <!-- #####  Referenced objects #####  -->
    <!-- ########################### -->
    <xsl:apply-templates select="sml:contact">
      <xsl:with-param name="Contact" select="$Contact"/>
    </xsl:apply-templates>
    <xsl:apply-templates select="sml:validTime">
      <xsl:with-param name="ValidTimeStart" select="$ValidTimeStart"/>
      <xsl:with-param name="ValidTimeEnd" select="$ValidTimeEnd"/>
    </xsl:apply-templates>
    <xsl:apply-templates select="sml:keywords">
      <xsl:with-param name="Keyword" select="$Keyword"/>
    </xsl:apply-templates>
    <xsl:if test="sml:identification/sml:IdentifierList/sml:identifier/@name = 'uniqueID' or sml:identification/sml:IdentifierList/sml:identifier/@name = 'longName' or sml:identification/sml:IdentifierList/sml:identifier/@name = 'shortName'">
      <xsl:apply-templates select="sml:identification">
        <xsl:with-param name="IdUnique" select="$IdUnique"/>
        <xsl:with-param name="IdLongName" select="$IdLongName"/>
        <xsl:with-param name="IdShortName" select="$IdShortName"/>
      </xsl:apply-templates>
    </xsl:if>
    <xsl:apply-templates select="sml:capabilities">
      <xsl:with-param name="MeasurementCapability" select="$MeasurementCapability"/>
    </xsl:apply-templates>
    <xsl:apply-templates select="sml:position">
      <xsl:with-param name="Deployment" select="$Deployment"/>
    </xsl:apply-templates>
    <xsl:apply-templates select="sml:classification">
      <xsl:with-param name="Classification" select="$Classification"/>
    </xsl:apply-templates>
    <xsl:if test="sml:inputs/sml:InputList/sml:input or sml:outputs/smlOutputList/sml:output">
      <xsl:call-template name="process">
        <xsl:with-param name="Process" select="$Process"/>
      </xsl:call-template>
    </xsl:if>
    <xsl:apply-templates select="sml:components">
      <xsl:with-param name="Component" select="$Component"/>
    </xsl:apply-templates>
  </xsl:template>
  <!-- ======================================================================== -->
  <!-- ============================== SUB templates ============================== -->
  <!-- ======================================================================== -->
  <!-- ********************************************** -->
  <!-- sml:keywords -->
  <!-- ********************************************** -->
  <xsl:template match="sml:keywords">
    <xsl:param name="Keyword"/>
    <xsl:element name="owl:NamedIndividual">
      <xsl:namespace name="rdf" select="'&rdf;'"/>
      <xsl:attribute name="rdf:about"><xsl:value-of select="$Keyword"/></xsl:attribute>
      <xsl:element name="rdf:type">
        <xsl:attribute name="rdf:resource">&keo;Keyword</xsl:attribute>
      </xsl:element>
      <xsl:for-each select="sml:KeywordList/sml:keyword">
        <xsl:element name="keo:value">
          <xsl:attribute name="rdf:datatype">&xsd;token</xsl:attribute>
          <xsl:value-of select="."/>
        </xsl:element>
      </xsl:for-each>
    </xsl:element>
  </xsl:template>
  <!-- ********************************************** -->
  <!-- sml:identification -->
  <!-- ********************************************** -->
  <xsl:template match="sml:identification">
    <xsl:param name="IdUnique"/>
    <xsl:param name="IdLongName"/>
    <xsl:param name="IdShortName"/>
    <xsl:for-each select="sml:IdentifierList/sml:identifier">
      <xsl:element name="owl:NamedIndividual">
        <xsl:namespace name="rdf" select="'&rdf;'"/>
        <xsl:choose>
          <xsl:when test="@name = 'uniqueID'">
            <xsl:attribute name="rdf:about"><xsl:value-of select="$IdUnique"/></xsl:attribute>
          </xsl:when>
          <xsl:when test="@name = 'longName'">
            <xsl:attribute name="rdf:about"><xsl:value-of select="$IdLongName"/></xsl:attribute>
          </xsl:when>
          <xsl:when test="@name = 'shortName'">
            <xsl:attribute name="rdf:about"><xsl:value-of select="$IdShortName"/></xsl:attribute>
          </xsl:when>
        </xsl:choose>
        <xsl:element name="rdf:type">
          <xsl:attribute name="rdf:resource">&ido;Identifier</xsl:attribute>
        </xsl:element>
        <xsl:element name="ido:name">
          <xsl:attribute name="rdf:datatype">&xsd;token</xsl:attribute>
          <xsl:value-of select="@name"/>
        </xsl:element>
        <xsl:element name="ido:definition">
          <xsl:attribute name="rdf:datatype">&xsd;anyURI</xsl:attribute>
          <xsl:value-of select="sml:Term/@definition"/>
        </xsl:element>
        <xsl:element name="ido:value">
          <xsl:attribute name="rdf:datatype">&xsd;token</xsl:attribute>
          <xsl:value-of select="sml:Term/sml:value"/>
        </xsl:element>
      </xsl:element>
    </xsl:for-each>
  </xsl:template>
  <!-- ********************************************** -->
  <!-- sml:classification -->
  <!-- ********************************************** -->
  <xsl:template match="sml:classification">
    <xsl:param name="Classification"/>
    <xsl:for-each select="sml:ClassifierList/sml:classifier">
      <xsl:element name="owl:NamedIndividual">
        <xsl:namespace name="rdf" select="'&rdf;'"/>
        <xsl:attribute name="rdf:about"><xsl:value-of select="concat($Classification,position())"/></xsl:attribute>
        <xsl:element name="rdf:type">
          <xsl:attribute name="rdf:resource">&clo;Classifier</xsl:attribute>
        </xsl:element>
        <xsl:element name="clo:name">
          <xsl:attribute name="rdf:datatype">&xsd;token</xsl:attribute>
          <xsl:value-of select="@name"/>
        </xsl:element>
        <xsl:element name="clo:definition">
          <xsl:attribute name="rdf:datatype">&xsd;anyURI</xsl:attribute>
          <xsl:value-of select="sml:Term/@definition"/>
        </xsl:element>
        <xsl:element name="clo:value">
          <xsl:attribute name="rdf:datatype">&xsd;token</xsl:attribute>
          <xsl:value-of select="sml:Term/sml:value"/>
        </xsl:element>
      </xsl:element>
    </xsl:for-each>
  </xsl:template>
  <!-- ********************************************** -->
  <!-- sml:validTime -->
  <!-- ********************************************** -->
  <xsl:template match="sml:validTime">
    <xsl:param name="ValidTimeStart"/>
    <xsl:param name="ValidTimeEnd"/>
    <xsl:element name="owl:NamedIndividual">
      <xsl:namespace name="rdf" select="'&rdf;'"/>
      <xsl:attribute name="rdf:about"><xsl:value-of select="$ValidTimeStart"/></xsl:attribute>
      <xsl:element name="rdf:type">
        <xsl:attribute name="rdf:resource">&DUL;Region</xsl:attribute>
      </xsl:element>
      <xsl:element name="DUL:hasDataValue">
        <xsl:attribute name="rdf:datatype">&xsd;dateTime</xsl:attribute>
        <xsl:value-of select="gml:TimePeriod/gml:beginPosition"/>
      </xsl:element>
    </xsl:element>
    <xsl:element name="owl:NamedIndividual">
      <xsl:namespace name="rdf" select="'&rdf;'"/>
      <xsl:attribute name="rdf:about"><xsl:value-of select="$ValidTimeEnd"/></xsl:attribute>
      <xsl:element name="rdf:type">
        <xsl:attribute name="rdf:resource">&DUL;Region</xsl:attribute>
      </xsl:element>
      <!-- FIXME: handle <gml:endPosition indeterminatePosition="unknown"/> -->
      <!--xsl:element name="DUL:hasDataValue">
        <xsl:attribute name="rdf:datatype">&xsd;dateTime</xsl:attribute>
        <xsl:value-of select="gml:TimePeriod/gml:endPosition"/>
      </xsl:element-->
    </xsl:element>
  </xsl:template>
  <!-- ********************************************** -->
  <!-- sml:capabilities -->
  <!-- ********************************************** -->
  <xsl:template match="sml:capabilities">
    <xsl:param name="MeasurementCapability"/>
    <xsl:for-each select="swe:DataRecord/swe:field">
      <xsl:choose>
        <xsl:when test="upper-case('observedBBOX') = upper-case(@name)">
          <xsl:variable name="EP" select="concat($MeasurementCapability,position(),'EP')"/>
          <xsl:variable name="EPLC" select="concat($MeasurementCapability,position(),'EPLC')"/>
          <xsl:variable name="EPUC" select="concat($MeasurementCapability,position(),'EPUC')"/>
          <xsl:element name="owl:NamedIndividual">
            <xsl:namespace name="rdf" select="'&rdf;'"/>
            <xsl:attribute name="rdf:about"><xsl:value-of select="concat($MeasurementCapability,position())"/></xsl:attribute>
            <xsl:element name="rdf:type">
              <xsl:attribute name="rdf:resource">&DUL;SpaceRegion</xsl:attribute>
            </xsl:element>
            <xsl:element name="geo:hasPoint">
              <xsl:attribute name="rdf:resource"><xsl:value-of select="$EPLC"/></xsl:attribute>
            </xsl:element>
            <xsl:element name="geo:hasPoint">
              <xsl:attribute name="rdf:resource"><xsl:value-of select="$EPUC"/></xsl:attribute>
            </xsl:element>
          </xsl:element>
          <xsl:element name="owl:NamedIndividual">
            <xsl:namespace name="rdf" select="'&rdf;'"/>
            <xsl:attribute name="rdf:about"><xsl:value-of select="$EPLC"/></xsl:attribute>
            <xsl:element name="rdf:type">
              <xsl:attribute name="rdf:resource">&geo;Point</xsl:attribute>
            </xsl:element>
            <xsl:for-each select="swe:Envelope/swe:lowerCorner/swe:Vector/swe:coordinate">
              <xsl:element name="geo:hasCoordinate">
                <xsl:attribute name="rdf:resource"><xsl:value-of select="concat($EPLC,position())"/></xsl:attribute>
              </xsl:element>
            </xsl:for-each>
          </xsl:element>
          <xsl:element name="owl:NamedIndividual">
            <xsl:namespace name="rdf" select="'&rdf;'"/>
            <xsl:attribute name="rdf:about"><xsl:value-of select="$EPUC"/></xsl:attribute>
            <xsl:element name="rdf:type">
              <xsl:attribute name="rdf:resource">&geo;Point</xsl:attribute>
            </xsl:element>
            <xsl:for-each select="swe:Envelope/swe:lowerCorner/swe:Vector/swe:coordinate">
              <xsl:element name="geo:hasCoordinate">
                <xsl:attribute name="rdf:resource"><xsl:value-of select="concat($EPUC,position())"/></xsl:attribute>
              </xsl:element>
            </xsl:for-each>
          </xsl:element>
          <xsl:for-each select="swe:Envelope/swe:lowerCorner/swe:Vector/swe:coordinate">
            <xsl:call-template name="swe:coordinate">
              <xsl:with-param name="URI" select="concat($EPLC,position())"/>
            </xsl:call-template>
          </xsl:for-each>
          <xsl:for-each select="swe:Envelope/swe:upperCorner/swe:Vector/swe:coordinate">
            <xsl:call-template name="swe:coordinate">
              <xsl:with-param name="URI" select="concat($EPUC,position())"/>
            </xsl:call-template>
          </xsl:for-each>
        </xsl:when>
        <xsl:otherwise>
          <xsl:element name="owl:NamedIndividual">
            <xsl:namespace name="rdf" select="'&rdf;'"/>
            <xsl:attribute name="rdf:about"><xsl:value-of select="concat($MeasurementCapability,position())"/></xsl:attribute>
            <xsl:element name="rdf:type">
              <xsl:attribute name="rdf:resource">&ssn;MeasurementProperty</xsl:attribute>
            </xsl:element>
            <xsl:element name="cao:name">
              <xsl:attribute name="rdf:datatype">&xsd;token</xsl:attribute>
              <xsl:value-of select="@name"/>
            </xsl:element>
            <xsl:element name="cao:definition">
              <xsl:attribute name="rdf:datatype">&xsd;anyURI</xsl:attribute>
              <xsl:value-of select="swe:Text/@definition"/>
            </xsl:element>
            <xsl:element name="cao:value">
              <xsl:attribute name="rdf:datatype">&xsd;token</xsl:attribute>
              <xsl:value-of select="swe:Text/swe:value"/>
            </xsl:element>
            <xsl:element name="cao:description">
              <xsl:attribute name="rdf:datatype">&xsd;token</xsl:attribute>
              <xsl:value-of select="swe:Text/gml:description"/>
            </xsl:element>
          </xsl:element>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>
  <!-- ********************************************** -->
  <!-- sml:Contact -->
  <!-- ********************************************** -->
  <xsl:template match="sml:contact">
    <xsl:param name="Contact"/>
    <xsl:element name="owl:NamedIndividual">
      <xsl:namespace name="rdf" select="'&rdf;'"/>
      <xsl:attribute name="rdf:about"><xsl:value-of select="$Contact"/></xsl:attribute>
      <xsl:element name="rdf:type">
        <xsl:attribute name="rdf:resource">&cio;Organization</xsl:attribute>
      </xsl:element>
      <xsl:element name="cio:gml-id">
        <xsl:attribute name="rdf:datatype">&xsd;token</xsl:attribute>
        <xsl:value-of select="sml:ResponsibleParty/@gml:id"/>
      </xsl:element>
      <xsl:element name="cio:name">
        <xsl:attribute name="rdf:datatype">&xsd;token</xsl:attribute>
        <xsl:value-of select="sml:ResponsibleParty/sml:organizationName"/>
      </xsl:element>
      <xsl:element name="cio:electronic-mail-address">
        <xsl:attribute name="rdf:datatype">&xsd;string</xsl:attribute>
        <xsl:value-of select="sml:ResponsibleParty/sml:contactInfo/sml:address/sml:electronicMailAddress"/>
      </xsl:element>
    </xsl:element>
  </xsl:template>
  <!-- ********************************************** -->
  <!-- sml:position -->
  <!-- ********************************************** -->
  <xsl:template match="sml:position">
    <xsl:param name="Deployment"/>
    <xsl:element name="owl:NamedIndividual">
      <xsl:namespace name="rdf" select="'&rdf;'"/>
      <xsl:attribute name="rdf:about"><xsl:value-of select="$Deployment"/></xsl:attribute>
      <xsl:element name="rdf:type">
        <xsl:attribute name="rdf:resource">&geo;Point</xsl:attribute>
      </xsl:element>
      <xsl:element name="geo:gml-id">
        <xsl:attribute name="rdf:datatype">&xsd;token</xsl:attribute>
        <xsl:value-of select="swe:Position/swe:location/swe:Vector/@gml:id"/>
      </xsl:element>
      <xsl:element name="geo:name">
        <xsl:attribute name="rdf:datatype">&xsd;token</xsl:attribute>
        <xsl:value-of select="@name"/>
      </xsl:element>
      <xsl:element name="geo:reference-frame">
        <xsl:attribute name="rdf:datatype">&xsd;anyURI</xsl:attribute>
        <xsl:value-of select="swe:Position/@referenceFrame"/>
      </xsl:element>
      <xsl:for-each select="swe:Position/swe:location/swe:Vector/swe:coordinate">
        <xsl:element name="geo:hasCoordinate">
          <xsl:attribute name="rdf:resource"><xsl:value-of select="concat($Deployment,position())"/></xsl:attribute>
        </xsl:element>
      </xsl:for-each>
    </xsl:element>
    <xsl:for-each select="swe:Position/swe:location/swe:Vector/swe:coordinate">
      <xsl:call-template name="swe:coordinate">
        <xsl:with-param name="URI" select="concat($Deployment,position())"/>
      </xsl:call-template>
    </xsl:for-each>
  </xsl:template>
  <!-- ********************************************** -->
  <!-- swe:coordinate -->
  <!-- ********************************************** -->
  <xsl:template name="swe:coordinate">
    <xsl:param name="URI"/>
    <xsl:element name="owl:NamedIndividual">
      <xsl:namespace name="rdf" select="'&rdf;'"/>
      <xsl:attribute name="rdf:about"><xsl:value-of select="$URI"/></xsl:attribute>
      <xsl:element name="rdf:type">
        <xsl:attribute name="rdf:resource">&geo;Coordinate</xsl:attribute>
      </xsl:element>
      <xsl:choose>
        <xsl:when test="upper-case('longitude') = upper-case(@name) or upper-case('easting') = upper-case(@name) or upper-case('westing') = upper-case(@name)">
          <xsl:element name="geo:longitude">
            <xsl:attribute name="rdf:datatype">&xsd;double</xsl:attribute>
            <xsl:value-of select="swe:Quantity/swe:value"/>
          </xsl:element>
        </xsl:when>
        <xsl:when test="upper-case('latitude') = upper-case(@name) or upper-case('northing') = upper-case(@name) or upper-case('southing') = upper-case(@name)">
          <xsl:element name="geo:latitude">
            <xsl:attribute name="rdf:datatype">&xsd;double</xsl:attribute>
            <xsl:value-of select="swe:Quantity/swe:value"/>
          </xsl:element>
        </xsl:when>
        <xsl:when test="upper-case('altitude') = upper-case(@name)">
          <xsl:element name="geo:altitude">
            <xsl:attribute name="rdf:datatype">&xsd;double</xsl:attribute>
            <xsl:value-of select="swe:Quantity/swe:value"/>
          </xsl:element>
        </xsl:when>
      </xsl:choose>
      <xsl:element name="geo:unit-of-measure">
        <xsl:attribute name="rdf:datatype">&xsd;token</xsl:attribute>
        <xsl:value-of select="swe:Quantity/swe:uom/@code"/>
      </xsl:element>
    </xsl:element>
  </xsl:template>
  <!-- ********************************************** -->
  <!-- process -->
  <!-- ********************************************** -->
  <xsl:template name="process">
    <xsl:param name="Process"/>
    <xsl:element name="owl:NamedIndividual">
      <xsl:namespace name="rdf" select="'&rdf;'"/>
      <xsl:attribute name="rdf:about"><xsl:value-of select="$Process"/></xsl:attribute>
      <xsl:element name="rdf:type">
        <xsl:attribute name="rdf:resource">&ssn;Process</xsl:attribute>
      </xsl:element>
      <xsl:for-each select="sml:inputs/sml:InputList/sml:input">
        <xsl:element name="ssn:hasInput">
          <xsl:attribute name="rdf:resource"><xsl:value-of select="concat($Process,'I',position())"/></xsl:attribute>
        </xsl:element>
      </xsl:for-each>
      <xsl:for-each select="sml:outputs/sml:OutputList/sml:output">
        <xsl:element name="ssn:hasOutput">
          <xsl:attribute name="rdf:resource"><xsl:value-of select="concat($Process,'O',position())"/></xsl:attribute>
        </xsl:element>
      </xsl:for-each>
    </xsl:element>
    <xsl:for-each select="sml:inputs/sml:InputList/sml:input">
      <xsl:element name="owl:NamedIndividual">
        <xsl:namespace name="rdf" select="'&rdf;'"/>
        <xsl:attribute name="rdf:about"><xsl:value-of select="concat($Process,'I',position())"/></xsl:attribute>
        <xsl:element name="rdf:type">
          <xsl:attribute name="rdf:resource">&ssn;Input</xsl:attribute>
        </xsl:element>
        <xsl:element name="pro:input-name">
          <xsl:attribute name="rdf:datatype">&xsd;token</xsl:attribute>
          <xsl:value-of select="@name"/>
        </xsl:element>
        <xsl:element name="pro:observable-property-definition">
          <xsl:attribute name="rdf:datatype">&xsd;anyURI</xsl:attribute>
          <xsl:value-of select="./swe:ObservableProperty/@definition"/>
        </xsl:element>
      </xsl:element>
    </xsl:for-each>
    <xsl:for-each select="sml:outputs/sml:OutputList/sml:output">
      <xsl:element name="owl:NamedIndividual">
        <xsl:namespace name="rdf" select="'&rdf;'"/>
        <xsl:attribute name="rdf:about"><xsl:value-of select="concat($Process,'O',position())"/></xsl:attribute>
        <xsl:element name="rdf:type">
          <xsl:attribute name="rdf:resource">&ssn;Output</xsl:attribute>
        </xsl:element>
        <xsl:element name="pro:output-name">
          <xsl:attribute name="rdf:datatype">&xsd;token</xsl:attribute>
          <xsl:value-of select="@name"/>
        </xsl:element>
        <xsl:element name="pro:quantity-definition">
          <xsl:attribute name="rdf:datatype">&xsd;anyURI</xsl:attribute>
          <xsl:value-of select="./swe:Quantity/@definition"/>
        </xsl:element>
        <xsl:element name="pro:unit-of-measure">
          <xsl:attribute name="rdf:datatype">&xsd;token</xsl:attribute>
          <xsl:value-of select="./swe:Quantity/swe:uom/@code"/>
        </xsl:element>
      </xsl:element>
    </xsl:for-each>
  </xsl:template>
  <!-- ********************************************** -->
  <!-- sml:components -->
  <!-- ********************************************** -->
  <xsl:template match="sml:components">
    <xsl:param name="Component"/>
    <xsl:for-each select="sml:ComponentList/sml:component">
      <xsl:apply-templates select="sml:Component">
        <xsl:with-param name="System" select="concat($Component,position())"/>
      </xsl:apply-templates>
    </xsl:for-each>
  </xsl:template>
  <!-- ********************************************** -->
  <!-- sml:Component -->
  <!-- ********************************************** -->
  <xsl:template match="sml:Component">
    <xsl:param name="System"/>
    <xsl:call-template name="Component">
      <xsl:with-param name="System" select="$System"/>
    </xsl:call-template>
  </xsl:template>
</xsl:stylesheet>
