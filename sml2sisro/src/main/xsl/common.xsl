<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions">

<!-- **************************************************************************************************** -->
<!-- Function definitions -->
<!-- **************************************************************************************************** -->
<!-- Spec: Substring before match -->
<xsl:function name="functx:substring-before-match" as="xs:string?" 
              xmlns:functx="http://www.functx.com" >
  <xsl:param name="arg" as="xs:string?"/> 
  <xsl:param name="regex" as="xs:string"/> 
  <xsl:sequence select="tokenize($arg,$regex)[1]"/>
</xsl:function>
<!-- Spec: Substring after match -->
<xsl:function name="functx:substring-after-match" as="xs:string?" 
              xmlns:functx="http://www.functx.com" >
  <xsl:param name="arg" as="xs:string?"/> 
  <xsl:param name="regex" as="xs:string"/> 
  <xsl:sequence select="replace($arg,concat('^.*?',$regex),'')"/> 
</xsl:function>
<!-- Spec: Create an identifier from a string by 
	1) deleting some unaccepted characters, 
	2) replacing accented characters with unaccented ones,
	3) tokenize string at whitespace characters,
	4) the first letter of the token - which index is > 1 - is capitalized-->
<xsl:function name="func:words-to-camel-case" as="xs:string" 
              xmlns:func="http://xsltfunc.multilogic.hu">
  <xsl:param name="arg" as="xs:string?"/> 
  <xsl:sequence select=" 
     string-join((tokenize(fn:translate(fn:translate($arg,',().§–!',''),'öüóőúéáűíÖÜÓŐÚÉÁŰÍ','ouooueauiOUOOUEAUI'),'\s+')[1],
       for $word in tokenize(fn:translate(fn:translate($arg,',().§–!',''),'öüóőúéáűíÖÜÓŐÚÉÁŰÍ','ouooueauiOUOOUEAUI'),'\s+')[position() > 1]
       return func:capitalize-first-letter($word))
      ,'')
 "/>
 </xsl:function>

<!-- Spec: Capitalize the first lettet of the input string -->
 <xsl:function name="func:capitalize-first-letter" as="xs:string?" 
              xmlns:func="http://xsltfunc.multilogic.hu">
  <xsl:param name="arg" as="xs:string?"/> 
  <xsl:sequence select=" 
   concat(upper-case(substring($arg,1,1)),
             substring($arg,2))
 "/>
</xsl:function>
<!-- **************************************************************************************************** -->
<!-- Template definitions -->
<!-- **************************************************************************************************** -->
<xsl:template name="prefix">
<xsl:text disable-output-escaping="yes">
@prefix dc: &lt;http://purl.org/dc/elements/1.1/&gt; .
@prefix metalex-owl-ext: &lt;http://purl.org/net/emerald/owl/metalex-owl-ext#&gt; .
@prefix owl: &lt;http://www.w3.org/2002/07/owl#&gt; .
@prefix rdf: &lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#&gt; .
@prefix xml: &lt;http://www.w3.org/XML/1998/namespace&gt; .
@prefix xsd: &lt;http://www.w3.org/2001/XMLSchema#&gt; .
@prefix rdfs: &lt;http://www.w3.org/2000/01/rdf-schema#&gt; .
@prefix skos: &lt;http://www.w3.org/2004/02/skos/core#&gt; .
@prefix dcterms: &lt;http://purl.org/dc/terms/&gt; .
@prefix person: &lt;http://purl.org/net/emerald/owl/person#&gt; .
@prefix cnt: &lt;http://www.w3.org/2011/content#&gt; .
@prefix oa: &lt;http://www.w3.org/ns/oa#&gt; .
@prefix xlink: &lt;http://www.w3.org/1999/xlink#&gt; .
@prefix time-modification: &lt;http://www.estrellaproject.org/lkif-core/time-modification.owl#&gt; .
</xsl:text>
</xsl:template>

</xsl:stylesheet>
