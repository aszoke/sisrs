#!/bin/bash
# Stardog bootsrap

echo " *** Creating a database in the stardog repository *** "
echo " Usage: "
echo "  1) download a package (e.g. 'example_20140330.zip') from the download section (i.e. https://bitbucket.org/aszoke/sisrs/downloads)"
echo "     to a directory under the stardog data directory i.e. 'STARDOG_HOME/examples/data/<package directory>' "
echo "  2) unzip the package"
echo "  3) run the scipt as 'bash sdb sisro'"
echo ""
echo " The following database will be created: $1"
echo ""

while true; do
  read -p " Do you wish to drop database $1 to create a new one?" yn
  case $yn in
    [Yy]* ) break;;
    [Nn]* ) exit;;
    * ) echo "Please answer yes or no.";;
  esac
done

echo ""
EXPORT_TIME_STAMP=$(date +"%Y%m%d%H%M%S")
EXPORT_NAME="sisro-export-$EXPORT_TIME_STAMP.nt"
echo " - Exporting database: $1 as -> '$EXPORT_NAME'"
bash ../../../bin/stardog data export --format NTRIPLES  -u admin -p g3s2Oneo sisro $EXPORT_NAME
# echo "EXPORT WAS SKIPPED!"
echo "Done."

echo ""
echo " - Dropping database: $1"
bash ../../../bin/stardog-admin db drop $1 -u admin -p g3s2Oneo
echo "Done."

echo ""
echo " - Creating database: $1"
bash ../../../bin/stardog-admin db create -n $1 -u admin -p g3s2Oneo
echo "Done."

echo ""
echo " - Load schemas, devices and applications:"
for filename in *.rdf; do
  echo "$filename"
  fname=$filename | cut -f1 -d'.'
  bash ../../../bin/stardog data add $1 $filename -u admin -p g3s2Oneo
done

echo ""
echo " - Inserting devices and applications:"
for filename in insert_*.ru; do
  echo "$filename"
  bash ../../../bin/stardog query $1 $filename -u admin -p g3s2Oneo
done

echo ""
echo " - Starting applications on devices:"
for filename in start_*.ru; do
  bash ../../../bin/stardog query $1 $filename -u admin -p g3s2Oneo
done

echo ""
echo " - The created database contains the following count of triples:"
bash ../../../bin/stardog query $1 "SELECT (COUNT(?s) as ?counts) { { ?s ?p ?o } UNION { GRAPH ?g { ?s ?p ?o } } }" -u admin -p g3s2Oneo

TIME_STAMP=$(date +"%x %R %Z")
echo ""
echo " *** The database $1 is successfully created at $TIME_STAMP by $USER. ***"
