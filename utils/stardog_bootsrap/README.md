# Notes on downloading #

1) Login the 'stardog' Ubuntu Linux

<code>
ssh fandrew@stardog
(passw: 'stardog')
</code>

2) Going to the data directory

<code>
/home/fandrew/stardog/examples/data/
</code>

3) Downloading a importable file from this repository.

Use the following form (it will requests the password for bitbucket):

<code>
curl -L -O -u <username> "https://bitbucket.org/aszoke/sisrs/downloads/<filename w ext>"
</code>

4) Unzip the file

<code>
unzip <filename.zip> -d . 
</code>

5) Running the import script

<code>
bash sdb_reloads.sh sisro
</code>
