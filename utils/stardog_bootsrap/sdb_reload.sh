#!/bin/bash
# Stardog bootsrap

echo " *** Reloading a database in the stardog repository *** "
echo " Usage: "
echo "  1) download a package (e.g. 'example_20140330.zip') from the download section (i.e. https://bitbucket.org/aszoke/sisrs/downloads)"
echo "     to a directory under the stardog data directory i.e. 'STARDOG_HOME/examples/data/<package directory>' "
echo "  2) unzip the package"
echo "  3) run the scipt as 'bash sdb_reload sisro'"
echo ""
echo " The following database will be reloaded: $1"
echo ""

while true; do
  read -p " Do you wish to clear database $1?" yn
  case $yn in
    [Yy]* ) break;;
    [Nn]* ) exit;;
    * ) echo "Please answer yes or no.";;
  esac
done

echo ""
EXPORT_TIME_STAMP=$(date +"%Y%m%d%H%M%S")
EXPORT_NAME="$1-export-$EXPORT_TIME_STAMP.nt"
echo " - Exporting database: $1 as -> '$EXPORT_NAME'"
bash ../../../bin/stardog data export --format NTRIPLES  -u admin -p g3s2Oneo $1 $EXPORT_NAME
# echo "EXPORT WAS SKIPPED!"
echo "Done."

echo ""
echo " - Clearing database: $1"
bash ../../../bin/stardog query $1 "DELETE WHERE {?s ?p ?o.}" -u admin -p g3s2Oneo
echo "Done."

echo ""
echo " - Load schemas, sensors, devices and applications:"
for filename in *.rdf; do
  echo "$filename"
  fname=$filename | cut -f1 -d'.'
  bash ../../../bin/stardog data add $1 $filename -u admin -p g3s2Oneo
done

echo ""
echo " - Inserting devices and applications:"
for filename in insert_*.ru; do
  echo "$filename"
  bash ../../../bin/stardog query $1 $filename -u admin -p g3s2Oneo
done

echo ""
echo " - Starting applications on devices:"
for filename in start_*.ru; do
  bash ../../../bin/stardog query $1 $filename -u admin -p g3s2Oneo
done

echo ""
echo " - The created database contains the following count of triples:"
bash ../../../bin/stardog query $1 "SELECT (COUNT(?s) as ?counts) { { ?s ?p ?o } UNION { GRAPH ?g { ?s ?p ?o } } }" -u admin -p g3s2Oneo

TIME_STAMP=$(date +"%x %R %Z")
echo ""
echo " *** The database $1 is successfully reloaded at $TIME_STAMP by $USER. ***"
