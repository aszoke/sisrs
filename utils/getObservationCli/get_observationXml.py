# -*- coding: utf-8 -*-
"""
SOS observation collector

Downloads and locally saves all observations available in the SOS corresponding
to the speicified procedures and observed properties. Plots the data.
"""

SOS_URL = 'http://152.66.253.152:8080/52n-sos-webapp/sos/pox'

TARGETS = [
		(
			'http://mit.bme.hu/sensorweb/procedures/beagle2_utilization',			# procedure
			'http://mit.bme.hu/sensorweb/observableProperty/node_utilization_cpu_us',	# observed property
			'GetObservation_template.xml',								# xml_template
			'beagle2_utilization_cpu_us.xml'									# xml_file
		),
		(
			'http://mit.bme.hu/sensorweb/procedures/beagle2_utilization',			# procedure
			'http://mit.bme.hu/sensorweb/observableProperty/node_utilization_cpu_sy',	# observed property
			'GetObservation_template.xml',								# xml_template
			'beagle2_utilization_cpu_sy.xml'									# xml_file
		),
		(
			'http://mit.bme.hu/sensorweb/procedures/beagle2_utilization',			# procedure
			'http://mit.bme.hu/sensorweb/observableProperty/node_utilization_cpu_tw',	# observed property
			'GetObservation_template.xml',								# xml_template
			'beagle2_utilization_cpu_tw.xml'									# xml_file
		),
		(
			'http://mit.bme.hu/sensorweb/procedures/beagle2_utilization',			# procedure
			'http://mit.bme.hu/sensorweb/observableProperty/node_utilization_cpu_wa',	# observed property
			'GetObservation_template.xml',								# xml_template
			'beagle2_utilization_cpu_wa.xml'									# xml_file
		),		
		(
			'http://mit.bme.hu/sensorweb/procedures/beagle2_utilization',			# procedure
			'http://mit.bme.hu/sensorweb/observableProperty/node_utilization_net_inoctets',	# observed property
			'GetObservation_template.xml',								# xml_template
			'beagle2_utilization_net_inocetets.xml'									# xml_file
		),				
		(
			'http://mit.bme.hu/sensorweb/procedures/beagle2_utilization',			# procedure
			'http://mit.bme.hu/sensorweb/observableProperty/node_utilization_net_outoctets',	# observed property
			'GetObservation_template.xml',								# xml_template
			'beagle2_utilization_net_outocetets.xml'									# xml_file
		),
		
		(
			'http://mit.bme.hu/sensorweb/procedures/beagle3_utilization',			# procedure
			'http://mit.bme.hu/sensorweb/observableProperty/node_utilization_cpu_us',	# observed property
			'GetObservation_template.xml',								# xml_template
			'beagle3_utilization_cpu_us.xml'									# xml_file
		),
		(
			'http://mit.bme.hu/sensorweb/procedures/beagle3_utilization',			# procedure
			'http://mit.bme.hu/sensorweb/observableProperty/node_utilization_cpu_sy',	# observed property
			'GetObservation_template.xml',								# xml_template
			'beagle3_utilization_cpu_sy.xml'									# xml_file
		),
		(
			'http://mit.bme.hu/sensorweb/procedures/beagle3_utilization',			# procedure
			'http://mit.bme.hu/sensorweb/observableProperty/node_utilization_cpu_tw',	# observed property
			'GetObservation_template.xml',								# xml_template
			'beagle3_utilization_cpu_tw.xml'									# xml_file
		),
		(
			'http://mit.bme.hu/sensorweb/procedures/beagle3_utilization',			# procedure
			'http://mit.bme.hu/sensorweb/observableProperty/node_utilization_cpu_wa',	# observed property
			'GetObservation_template.xml',								# xml_template
			'beagle3_utilization_cpu_wa.xml'									# xml_file
		),		
		(
			'http://mit.bme.hu/sensorweb/procedures/beagle3_utilization',			# procedure
			'http://mit.bme.hu/sensorweb/observableProperty/node_utilization_net_inoctets',	# observed property
			'GetObservation_template.xml',								# xml_template
			'beagle3_utilization_net_inocetets.xml'									# xml_file
		),				
		(
			'http://mit.bme.hu/sensorweb/procedures/beagle3_utilization',			# procedure
			'http://mit.bme.hu/sensorweb/observableProperty/node_utilization_net_outoctets',	# observed property
			'GetObservation_template.xml',								# xml_template
			'beagle3_utilization_net_outocetets.xml'									# xml_file
		)		
	]

## DO NOT EDIT BELOW THIS LINE

import sys
import re
import time
import os.path
from datetime import datetime
import urllib2
import glob

PROCEDURE 			= 0
OBSERVED_PROPERTY 	= 1
XML_TEMPLATE		= 2
XML_FILE			= 3

#########################################
### FUNCTION DEFINITIONS
#########################################

def BuildRequest(template, procedure, observed_property, last_timestamp):
	request_string = ''
	with open(template, 'r') as template_string:
		request_string = template_string.read()
		
	timestamp = last_timestamp;
	formatted_timestamp = (datetime.fromtimestamp(timestamp)).strftime("%Y-%m-%dT%H:%M:%S+01:00")
	request_string = request_string.replace('$_procedure', procedure)
	request_string = request_string.replace('$_observedProperty', observed_property)
	request_string = request_string.replace('$_timeFrom', formatted_timestamp)
	request_string = request_string.replace('$_timeTo', time.strftime('%Y-%m-%dT%H:%M:%S+01:00', time.localtime()))
	
	return request_string
	
def SosRequest(url, query):
	request = urllib2.Request(url, data = query, headers = {'Content-Type': 'application/xml'})
	connection = urllib2.urlopen(request)
	
	return connection.read()

#########################################
### MAIN
#########################################
def main(argv=None):
	if argv is None:
		argv = sys.argv

	reqi = 0
	for target in TARGETS:	
		reqi = reqi + 1
		xml_request = BuildRequest(target[XML_TEMPLATE], target[PROCEDURE], target[OBSERVED_PROPERTY], 0)
		
		print('-> Request: ' +`reqi`)
		t0= time.clock()
		xml_result = SosRequest(SOS_URL, xml_request)
		print('  Done. It took: ' + `time.clock() - t0` + ' seconds.')
		
		#print('----')
		#print(xml_result)
		#print('----')
		
		f = open(target[XML_FILE],'w')
		f.write(xml_result) # python will convert \n to os.linesep
		f.close() # you can omit in most cases as the destructor will call if	 
		print('  Saved to file: ' + target[XML_FILE])
		
#########################################
### MAIN GUARD
#########################################
if __name__ == "__main__":
	main()
