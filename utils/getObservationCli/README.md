# GetObservationCli

## About

This tiny utility is used to query observation data through command line interface. The queried data identifiers are in the header of the python file.

(Note: this utility is a simplified version of the 'get_observation' utility that was given to me by Attila Szarvas.)

### Features

* querying SOS observation from SOS server and serializing the data (XML)

#### Status

See the detailed [issue list](https://bitbucket.org/aszoke/sisrs/issues?status=new&status=open) for details.

### Dependencies

* Python 2.7

#### Related projects

* [sisrs](https://bitbucket.org/aszoke/sisrs)

## Documentation and Installation Instructions

An extensive documentation for users (installation instructions, troubleshooting, ...) is available in the *TBD*.

## Usage

### Example

Querying available observation data

    python get_observationXML

### Test Data

 -