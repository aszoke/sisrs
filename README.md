# Sensor Instance Semantic Registry (SISR) 

## About

The Sensor Instance Semantic Registry (SISR) is a service interface for managing sensor catalogues. 

### Features

* validating schematron rule file (*.sch) with iso-schematron.rng
* validating RDF/XML file with rdfxml.rnc
* validating SensorML XML document with schematron rules to check compliance with Discovery Profile (sensorMLProfileForDiscovery.sch)
* generating documentation from OWL files using templates
* converting between RDF syntaxes

#### Status

See the detailed [issue list](https://bitbucket.org/aszoke/sisrs/issues?status=new&status=open) for details.

### Dependencies

* Apache Ant 1.9.x + Ant Contlib
* Saxon 9
* Jing
* Python 2.7
* RDF2RDF
* JRE 7

#### Related projects

* [Specgen6sw](https://bitbucket.org/aszoke/specgen6sw)

## Documentation and Installation Instructions

An extensive documentation for users (installation instructions, troubleshooting, ...) is available in the *TBD*.

## Usage

### Example

Validating SensorML XML document

    ant smlvalidate

Converting TTL to RDF/XML document

    ant rdf2rdf

Generating documentation of OWL ontologies

    ant owldocgen

Copying *.html and *.rdf files to an Apache HTTP Server's 'htdocs' (document root) directory

    ant copy2htdocs

Cleaning the workspace

    ant clean

See [build.xml](https://.../build.xml) for details.

### Test Data

SensorML instance:

* [wheather-station.xml](https://.../wheather-station.xml)

Instance ontologies:

* [i_cell-phones.ttl](https://.../i_cell-phones.ttl)
* [i_weather-station.ttl](https://.../i_weather-station.ttl)

### Test server

A test instance for version 0.1 is available at *TBD*

Testing the content negotiation:

    wget localhost:8080/sisro/geo --header="Accept: text/html"
    wget localhost:8080/sisro/geo --header="Accept: application/rdf+xml"

## License

 (The MIT License)

 Copyright (c) 2014 Akos Szoke

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 'Software'), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
