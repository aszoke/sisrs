declare default element namespace "http://www.opengis.net/sensorML/1.0.1";
declare namespace swe="http://www.opengis.net/swe/1.0.1";
declare namespace cps="http://mit.bme.hu/cps/";
declare namespace rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace owl="http://www.w3.org/2002/07/owl#";

import module namespace geoloc    = "http://mit.bme.hu/cps/geoloc" at "query/geoloc.xq";
import module namespace validTime = "http://mit.bme.hu/cps/validTime" at "query/validTime.xq";

declare variable $doc as document-node(element(SensorML)) external;

let $system := $doc/SensorML/member/System
(:
SOS server 3.5: urn:ogc:def:identifier:OGC:uniqueID
SOS server 4.0: urn:ogc:def:identifier:OGC:1.0:uniqueID
:)
let $id := $system/identification/IdentifierList/identifier/Term[@definition="urn:ogc:def:identifier:OGC:1.0:uniqueID"]/value/text()
(: TODO: fail if could not find $id :) 

(:	cps:addRdfStatement($id, "rdf:type", "cps:Sensor") :)

return
	<rdf:RDF>
	    <owl:Ontology>
	        <owl:imports rdf:resource="http://mit.bme.hu/cps/sensor-sample-schema.owl"/>
	    </owl:Ontology>
		<cps:sensor rdf:about="{$id}">
			{ geoloc:process($system) }
			{ validTime:process($system) }
		</cps:sensor>
	</rdf:RDF>

(:
  Example input:
  
      <sml:identification>
        <sml:IdentifierList>
          <sml:identifier>
            <sml:Term definition="urn:ogc:def:identifier:OGC:uniqueID">
              <sml:value>beagle3_OutOctets</sml:value>
            </sml:Term>
          </sml:identifier>
        </sml:IdentifierList>
      </sml:identification>
:)
