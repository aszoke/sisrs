module namespace validTime = "http://mit.bme.hu/cps/validTime";

declare default element namespace "http://www.opengis.net/sensorML/1.0.1";
declare namespace gml="http://www.opengis.net/gml";
declare namespace cps="http://mit.bme.hu/cps/";

declare function validTime:process($system as element(System)) as element(cps:valid)
{
	let $period := $system/validTime/gml:TimePeriod
	let $begin := $period/gml:beginPosition/text()
	let $end   := $period/gml:endPosition/text()
	
	return
		<cps:valid><cps:begin>{$begin}</cps:begin><cps:end>{$end}</cps:end></cps:valid>
};

(:
  Example input:

      <sml:validTime>
        <gml:TimePeriod>
          <gml:beginPosition>2014-02-20T15:09:38.000+01:00</gml:beginPosition>
          <gml:endPosition>2014-02-24T17:25:03.000+01:00</gml:endPosition>
        </gml:TimePeriod>
      </sml:validTime>
:)
