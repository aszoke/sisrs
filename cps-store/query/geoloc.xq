module namespace geoloc = "http://mit.bme.hu/cps/geoloc";

declare default element namespace "http://www.opengis.net/sensorML/1.0.1";
declare namespace swe="http://www.opengis.net/swe/1.0.1";
declare namespace cps="http://mit.bme.hu/cps/";
declare namespace rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#";

declare function geoloc:convertCoordinate($coord as element(swe:coordinate)) as element(cps:hasCoord)
{
	let $name := $coord/@name
	let $value := $coord/swe:Quantity/swe:value/text()
	return
		<cps:hasCoord>
			<cps:coord>
				<cps:hasDimension rdf:resource="{$name}"/>
				<cps:hasValue rdf:datatype="http://www.w3.org/2001/XMLSchema#decimal">{$value}</cps:hasValue>
			</cps:coord>
		</cps:hasCoord>
};

declare function geoloc:process($system as element(System)) as element(cps:hasLoc)
{
	let $pos := $system/position[@name="sensorPosition"]
	let $coords := $pos//swe:coordinate
	
	return
		<cps:hasLoc>
			<cps:loc>
				{for $coord in $coords return geoloc:convertCoordinate($coord)}
			</cps:loc>
		</cps:hasLoc>
};

(:
  Example input:

      <sml:position name="sensorPosition">
        <swe:Position referenceFrame="urn:ogc:def:crs:EPSG::4326">
          <swe:location>
            <swe:Vector gml:id="STATION_LOCATION">
              <swe:coordinate name="easting">
                <swe:Quantity>
                  <swe:uom code="degree"/>
                  <swe:value>7.52</swe:value>
                </swe:Quantity>
              </swe:coordinate>
              <swe:coordinate name="northing">
                <swe:Quantity>
                  <swe:uom code="degree"/>
                  <swe:value>52.90</swe:value>
                </swe:Quantity>
              </swe:coordinate>
              <swe:coordinate name="altitude">
                <swe:Quantity>
                  <swe:uom code="m"/>
                  <swe:value>52.0</swe:value>
                </swe:Quantity>
              </swe:coordinate>
            </swe:Vector>
          </swe:location>
        </swe:Position>
      </sml:position>
:)
