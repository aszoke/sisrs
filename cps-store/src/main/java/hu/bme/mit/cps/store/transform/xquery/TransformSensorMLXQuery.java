package hu.bme.mit.cps.store.transform.xquery;

import hu.bme.mit.cps.store.CPSVocabulary;
import hu.bme.mit.cps.store.transform.TransformSensorML2OWL;
import hu.bme.mit.cps.util.FileUtil;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import javax.xml.namespace.QName;
import javax.xml.xquery.XQConnection;
import javax.xml.xquery.XQException;
import javax.xml.xquery.XQItem;
import javax.xml.xquery.XQPreparedExpression;
import javax.xml.xquery.XQSequence;

import net.sf.saxon.xqj.SaxonXQDataSource;

public class TransformSensorMLXQuery implements TransformSensorML2OWL {
	public static final String XQUERY_PATH = "query/SensorML2RDF.xq";

	final XQConnection xqc;
	final XQPreparedExpression expr;

	public TransformSensorMLXQuery() throws XQException, IOException {
		SaxonXQDataSource sds = new SaxonXQDataSource();
		// register Java functions
		xqc = sds.getConnection();
		String query = FileUtil.readFile(XQUERY_PATH, StandardCharsets.UTF_8);
		expr = xqc.prepareExpression(query);
		expr.getStaticContext();
	}

	public String transform(String sensorID, String sensorML) throws XQException,
			IOException {
		XQItem item = xqc.createItemFromDocument(sensorML, null, null);
		QName rootNode = item.getItemType().getNodeName();
		if (rootNode.equals(CPSVocabulary.SML_SensorML)) {
			expr.bindItem(new QName("doc"), item);
			XQSequence result = expr.executeQuery();
			try {
				return result.getSequenceAsString(null);
			} finally {
				result.close();
			}
		} else if (rootNode.equals(CPSVocabulary.OWS_ExceptionReport)) {
			// error querying sensor data
			System.err.println("Skipping sensor " + sensorID
					+ ": exception from server.");
			return null;
		} else {
			System.err.println("Skipping sensor " + sensorID
					+ ": illegal SensorML root node '" + rootNode.getLocalPart() + "'.");
			return null;
		}
	}

	public void dispose() throws XQException {
		expr.close();
		xqc.close();
	}
}
