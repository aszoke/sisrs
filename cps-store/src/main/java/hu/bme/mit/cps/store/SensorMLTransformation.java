package hu.bme.mit.cps.store;

import hu.bme.mit.cps.store.transform.TransformSensorML2OWL;
import hu.bme.mit.cps.store.transform.xslt.TransformSensorMLXSLT;
import hu.bme.mit.cps.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.openrdf.model.Model;
import org.openrdf.model.Resource;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.ValueFactoryImpl;
import org.openrdf.repository.RepositoryException;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.Rio;
import org.semanticweb.HermiT.Configuration;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.io.FileDocumentTarget;
import org.semanticweb.owlapi.io.OWLFunctionalSyntaxOntologyFormat;
import org.semanticweb.owlapi.io.OWLOntologyDocumentSource;
import org.semanticweb.owlapi.io.StringDocumentSource;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.MissingImportHandlingStrategy;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyIRIMapper;
import org.semanticweb.owlapi.model.OWLOntologyLoaderConfiguration;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.util.OWLOntologyMerger;
import org.semanticweb.owlapi.vocab.PrefixOWLOntologyFormat;

import com.clarkparsia.owlapi.explanation.BlackBoxExplanation;
import com.clarkparsia.owlapi.explanation.SingleExplanationGenerator;

public class SensorMLTransformation {

	@SuppressWarnings("serial")
	public static class SensorMLTransformationException extends Exception {
		public SensorMLTransformationException(String msg) {
			super(msg);
		}

		public SensorMLTransformationException(String msg, Throwable e) {
			super(msg, e);
		}
	}

	// @formatter:off
	final static List<String> PRELOAD_ONTOLOGIES = Arrays.asList(
			"../sisro/src/main/owl/DUL.owl", 
			"../sisro/src/main/owl/application.ttl", 
			"../sisro/src/main/owl/capability.ttl", 
			"../sisro/src/main/owl/classification.ttl",
			"../sisro/src/main/owl/contact.ttl",
			"../sisro/src/main/owl/dedo.ttl",
			"../sisro/src/main/owl/geoposition.ttl",
			"../sisro/src/main/owl/identification.ttl",
			"../sisro/src/main/owl/keyword.ttl",
			"../sisro/src/main/owl/observation.ttl",
			"../sisro/src/main/owl/process.ttl",
			"../sisro/src/main/owl/time.ttl",
			"../sisro/src/main/owl/sisro.ttl",
			"../sisro/src/main/owl/ssn.ttl"
	);
	// @formatter:on
	final static String RDF_BASE_URI = "http://mit.bme.hu/sensorweb/";

	final TransformSensorML2OWL transf;
	final OWLOntologyManager manager;

	public SensorMLTransformation() throws Exception {
		transf = new TransformSensorMLXSLT();
		manager = OWLManager.createOWLOntologyManager();
		for (String ontologyFilename : PRELOAD_ONTOLOGIES) {
			System.err.println("Preloading ontology '" + ontologyFilename
					+ "'...");
			manager.loadOntologyFromOntologyDocument(new File(ontologyFilename));
		}
	}

	public void dispose() throws Exception {
		transf.dispose();
	}

	public Model transform(String sensorID, String sensorML)
			throws SensorMLTransformationException, IOException,
			RepositoryException {
		final String filename = sensorID
				.substring(sensorID.lastIndexOf('/') + 1);

		final String sensorOWL;
		try {
			sensorOWL = transf.transform(sensorID, sensorML);
			// FIXME: general exception
		} catch (Exception e) {
			throw new SensorMLTransformationException(
					"Error running transformation.", e);
		}
		FileUtil.writeFile("export/" + filename + ".owl", sensorOWL,
				StandardCharsets.UTF_8);

		OWLOntologyDocumentSource src = new StringDocumentSource(sensorOWL);
		OWLOntologyLoaderConfiguration config = new OWLOntologyLoaderConfiguration();
		// TODO: find a way to prohibit unknown ontology imports
		config.setMissingImportHandlingStrategy(MissingImportHandlingStrategy.SILENT);
		manager.addIRIMapper(new OWLOntologyIRIMapper() {

			@Override
			public IRI getDocumentIRI(IRI ontologyIRI) {
				System.err.println("OWL API is mapping IRI " + ontologyIRI);
				return null;
			}
		});
		OWLOntology onto;
		try {
			onto = manager.loadOntologyFromOntologyDocument(src, config);
		} catch (OWLOntologyCreationException e) {
			throw new SensorMLTransformationException(
					"Error reading transformed ontology.", e);
		}
		try {
			PrefixOWLOntologyFormat ontoFormat = new OWLFunctionalSyntaxOntologyFormat();
			// ontoFormat.copyPrefixesFrom(prefixManager);
			OWLOntologyManager manager2 = OWLManager.createOWLOntologyManager();
			OWLOntology merge = new OWLOntologyMerger(manager)
					.createMergedOntology(manager2, onto.getOntologyID()
							.getOntologyIRI());
			manager.saveOntology(merge, ontoFormat, new FileDocumentTarget(
					new File("export/" + filename + ".full.owl")));
		} catch (OWLException e) {
			System.err.println("Could not save merged ontology: "
					+ e.getMessage());
		}
		try {
			System.out.println("Ontology ID: " + onto.getOntologyID());
			System.out.println("Axioms: " + onto.getAxiomCount());
			for (OWLAxiom ax : onto.getAxioms()) {
				if (ax.getAxiomType() == AxiomType.CLASS_ASSERTION) {
				} else if (ax.getAxiomType() == AxiomType.OBJECT_PROPERTY_ASSERTION) {
				} else if (ax.getAxiomType() == AxiomType.DATA_PROPERTY_ASSERTION) {
				} else if (ax.getAxiomType() == AxiomType.DECLARATION) {
				} else if (ax.getAxiomType() == AxiomType.ANNOTATION_ASSERTION) {
					// only allow rdfs:comment and such
				} else {
					// Error in transformation, as all axioms must fall into
					// prior categories. AnnotationAxioms indicate that
					// Object/DataPropertyAssertions reference unknown
					// properties.
					throw new SensorMLTransformationException(
							"Illegal axiom type generated: " + ax.toString());
				}
			}

			try {
				// check ontology consistency
				OWLReasonerFactory fac = new org.semanticweb.HermiT.Reasoner.ReasonerFactory();
				// PelletReasonerFactory.getInstance();
				Configuration configuration=new Configuration();
		        configuration.throwInconsistentOntologyException=false;
				OWLReasoner reasoner = fac.createReasoner(onto, configuration);

				if (!reasoner.isConsistent()) {
					System.out.println("BTW ontology is inconsistent...");
					// TODO: consistency check
					SingleExplanationGenerator eg = new BlackBoxExplanation(
							onto, fac, reasoner);
//					ExplanationProgressMonitor progress = new SilentExplanationProgressMonitor();
//					ExplanationGenerator eg = new DefaultExplanationGenerator(
//							manager, fac, onto, progress);
					Set<OWLAxiom> expl = eg.getExplanation(manager
							.getOWLDataFactory().getOWLThing());
					throw new SensorMLTransformationException(
							"Ontology inconsistent:" + expl);
				}
			} catch (Exception e) {
				// throw new SensorMLTransformationException(
				// "Error checking ontology consistency.", e);
				System.err
						.println("Warning: error checking ontology consistency.");
				e.printStackTrace();
			}

			// AFAIK there is no way to convert OWL into RDF directly, the OWL
			// API classes incapable of this are RDFGraph and RDFXMLRenderer.
			// We have to parse the RDF/XML again using Sesame.
			// FIXME: Maybe it is possible to parse OWL from Sesame model?

			Reader source = new StringReader(sensorOWL);
			try {
				ValueFactory factory = ValueFactoryImpl.getInstance();
				Resource graphID = factory.createURI(onto.getOntologyID()
						.getOntologyIRI().toString());
				try {
					return Rio.parse(source, RDF_BASE_URI, RDFFormat.RDFXML,
							graphID);
				} catch (RDFParseException e) {
					throw new SensorMLTransformationException(
							"Sesame could not parse ontology.", e);
				}
			} finally {
				source.close();
			}
		} finally {
			manager.removeOntology(onto);
		}
	}
}
