package hu.bme.mit.cps.store;

public class StorageException extends Exception {

	private static final long serialVersionUID = 9153377982857871926L;

	public StorageException(String msg) {
		super(msg);
	}

	public StorageException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
