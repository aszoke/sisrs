package hu.bme.mit.cps.store;

import hu.bme.mit.cps.sos.Query;
import hu.bme.mit.cps.sos.SOSQuery;
import hu.bme.mit.cps.sos.SOSQueryJSON;
import hu.bme.mit.cps.util.FileUtil;

import java.io.File;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.openrdf.model.Model;
import org.semanticweb.owlapi.model.IRI;

public class UploadSensors {
	public static final URI SOS_WEBAPP = Query.TEST_SOS40_SERVER_WEBAPP;
	//public static final String STARDOG_URL = "http://cps1:5820/sisro";
	public static final String STARDOG_URL = "http://web.multilogic.hu:5820/sisro";
	
	public static final boolean SAVE_IN_STARDOG = false;

	public static void run(Iterable<ModelStorage> storage) throws Exception {
		SOSQuery sq = new SOSQuery(SOS_WEBAPP);
		SOSQueryJSON sqj = new SOSQueryJSON(SOS_WEBAPP);

		for (final String sensorID : sq.getSensors()) {
			SensorMLTransformation t = new SensorMLTransformation();
			String filename = sensorID.substring(sensorID
					.lastIndexOf('/') + 1);
			filename = filename.replace(':', '_');

			final String sensorML = sqj.x20DescribeSensor(sensorID);
			FileUtil.writeFile("export/" + filename + ".xml", sensorML,
					StandardCharsets.UTF_8);

			try {
				Model m = t.transform(sensorID, sensorML);
				System.out.println("RDF triples: " + m.size());
				for (ModelStorage s : storage) {
					s.save(IRI.create(sensorID), m);
				}
			} catch (Exception e) {
				if (e.getMessage() != null) {
					System.err.println("Fail: " + e.getMessage());
					e.printStackTrace();
				} else {
					System.err.println("Fail: stack trace below.");
					e.printStackTrace();
				}
			}
		}
	}

	public static void main(String args[]) throws Exception {
		List<ModelStorage> storage = new ArrayList<>();
		if (SAVE_IN_STARDOG) {
			storage.add(new StardogStorage(STARDOG_URL));
		}
		storage.add(new FileStorage(new File("export/")));
		try {
			run(storage);
		} finally {
			for (ModelStorage s : storage) {
				try {
					s.close();
				} catch (StorageException e) {
					System.err.println("Error closing storage: "
							+ e.getMessage());
				}
			}
		}
	}
}
