package hu.bme.mit.cps.store.transform.xslt;

import hu.bme.mit.cps.store.transform.TransformSensorML2OWL;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class TransformSensorMLXSLT implements TransformSensorML2OWL {
	public static final String XSLT_PATH = "../sml2sisro/src/main/xsl/sml2sisro.xsl";

	final Transformer expr;

	public TransformSensorMLXSLT() throws TransformerConfigurationException, TransformerFactoryConfigurationError {
		expr = TransformerFactory.newInstance().newTransformer(
				new StreamSource(new File(XSLT_PATH)));
	}

	public String transform(String sensorID, String sensorML) throws IOException, TransformerException {
		StringWriter result = new StringWriter();
		try {
			expr.transform(
					new StreamSource(new StringReader(sensorML)),
					new StreamResult(result));
			return result.toString();
		} finally {
			result.close();
		}
	}

	public void dispose() {
	}
}
