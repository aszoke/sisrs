package hu.bme.mit.cps.store;

import org.openrdf.model.Model;
import org.semanticweb.owlapi.model.IRI;

interface ModelStorage extends AutoCloseable {
	void save(IRI uri, Model m) throws StorageException;
	void close() throws StorageException;
}
