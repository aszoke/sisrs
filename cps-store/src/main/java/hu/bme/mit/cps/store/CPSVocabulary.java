package hu.bme.mit.cps.store;

import javax.xml.namespace.QName;

public class CPSVocabulary {

	public static final String NS_CPS = "http://mit.bme.hu/cps/";
	public static final String NS_SML = "http://www.opengis.net/sensorML/1.0.1";
	public static final String NS_OWS = "http://www.opengis.net/ows/1.1";

	public static final QName SML_SensorML = new QName(NS_SML, "SensorML");
	public static final QName OWS_ExceptionReport = new QName(NS_OWS, "ExceptionReport");
}
