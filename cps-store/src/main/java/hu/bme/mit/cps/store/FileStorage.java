package hu.bme.mit.cps.store;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;

import org.openrdf.model.Model;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.Rio;
import org.semanticweb.owlapi.model.IRI;

public class FileStorage implements ModelStorage {

	File directory;

	public FileStorage(File directory) throws StorageException {
		if (!directory.isDirectory()) {
			throw new StorageException("Not a directory: " + directory);
		}
		this.directory = directory;
	}

	@Override
	public void close() throws StorageException {
	}

	Random rnd = new Random();

	@Override
	public void save(IRI uri, Model m) throws StorageException {
		try {
			String filename = uri.getFragment();
			if (filename == null) {
				filename = uri.toString();
				filename = filename.replaceFirst("/*$", "");
				filename = filename.replaceFirst("\\*$", "");
				filename = filename.replaceFirst(".*/", "");
				filename = filename.replaceFirst(".*\\\\", "");
			}
			filename += ".rdf";
			// http://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html
			try (OutputStream out = new FileOutputStream(new File(directory,
					filename))) {
				Rio.write(m, out, RDFFormat.N3);
			}
		} catch (RDFHandlerException | IOException e) {
			throw new StorageException("Could not save model to file: " + e.getMessage(), e);
		}
	}
}
