package hu.bme.mit.cps.store;

import java.util.List;
import java.util.Random;

import org.openrdf.model.Model;
import org.openrdf.model.Resource;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.semanticweb.owlapi.model.IRI;

import com.complexible.stardog.sesame.StardogRepository;

public class StardogStorage implements ModelStorage {

	Repository repo = null;
	RepositoryConnection conn = null;

	public StardogStorage(String connection) throws StorageException {
		// Create a Sesame Repository from Stardog.
		repo = new StardogRepository(connection);
		try {
			repo.initialize();
			conn = repo.getConnection();
			System.out.println("Stardog stats:");
			System.out.printf("  %d statements\n", conn.size());
			// Stardog bugs prevent us from doing this {@link SesameAPITest}
			@SuppressWarnings("deprecation")
			final List<Resource> ctxs = conn.getContextIDs().asList();
			System.out.printf("  %d named graphs:\n", ctxs.size());
			for (Resource ctx : ctxs) {
				System.out.printf("  - [%d] %s\n", conn.size(ctx), ctx.toString());
			}
		} catch (RepositoryException e) {
			throw new StorageException(
					"Could not initialize Stardog repository.", e);
		}
	}

	@Override
	public void close() throws StorageException {
		try {
			if (conn != null) {
				conn.close();
			}
			conn = null;
		} catch (RepositoryException e) {
			throw new StorageException("Could not close Stardog connection.", e);
		}
	}

	Random rnd = new Random();

	@Override
	public void save(IRI uri, Model m) throws StorageException {
		final ValueFactory factory = repo.getValueFactory();
		try {
			URI context = factory.createURI(uri.toString());
			conn.begin();
			try {
//				final long oldSize = conn.size(context);
				conn.clear(context);
				conn.add(m, context);
//				final long newSize = conn.size(context);
				conn.commit();
				// Stardog bugs prevent us from doing this {@link SesameAPITest}
//				System.out.printf(
//						"Model %s saved to Stardog, context size: %d -> %d.\n",
//						uri.toString(), oldSize, newSize);
				System.out.printf(
						"Model %s saved to Stardog.\n",
						uri.toString());
			} finally {
				if (conn.isActive())
					conn.rollback();
			}
		} catch (RepositoryException e) {
			throw new StorageException("Could not save model to Stardog: "
					+ e.getMessage(), e);
		}
	}

	void test() throws RepositoryException {
		final ValueFactory factory = repo.getValueFactory();

		System.err.println("SIZE: " + conn.size());
		conn.begin();
		conn.add(factory.createURI("http://mit.bme.hu/CPS#res"),
				factory.createURI("http://mit.bme.hu/CPS#prop"),
				factory.createLiteral(2));
		conn.commit();
		System.err.println("SIZE: " + conn.size());
		conn.close();
	}

	public static void main(String args[]) throws Exception {
		final String stardogConnection = 
				//"http://cps-ro:v3LCWo5k@stardog:5820/sisro/";
				"http://cps-ro:v3LCWo5k@cps1:5820/sisro/";
		try (StardogStorage conn = new StardogStorage(stardogConnection)) {
			//conn.test();
		}
	}
}
