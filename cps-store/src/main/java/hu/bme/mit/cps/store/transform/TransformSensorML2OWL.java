package hu.bme.mit.cps.store.transform;


public interface TransformSensorML2OWL {

	String transform(String sensorID, String sensorML) throws Exception;
	public void dispose() throws Exception;
}
