package hu.bme.mit.cps.store.transform.xslt;

import hu.bme.mit.cps.store.SensorML2OWLTest;
import hu.bme.mit.cps.store.transform.TransformSensorML2OWL;

public class TransformSensorMLXSLTTest extends SensorML2OWLTest {

	@Override
	protected TransformSensorML2OWL setUpTransform() throws Exception {
		return new TransformSensorMLXSLT();
	}
}
