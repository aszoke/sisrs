package hu.bme.mit.cps.store;

import hu.bme.mit.cps.sos.Query;
import hu.bme.mit.cps.sos.SOSQuery;
import hu.bme.mit.cps.store.transform.TransformSensorML2OWL;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public abstract class SensorML2OWLTest {

	SOSQuery sos;
	TransformSensorML2OWL transf;
	
	protected abstract TransformSensorML2OWL setUpTransform() throws Exception;
	
	@Before
	public void setUp() throws Exception {
		sos = new SOSQuery(Query.TEST_SOS40_SERVER_URI);
		transf = setUpTransform();
	}

	@After
	public void tearDown() throws Exception {
		transf.dispose();
	}

	@Test
	public void testTransform() throws Exception {
		for (String sensor : sos.getSensors()) {
			final String sensorML = sos.describeSensor(sensor);
			final String sensorOWL = transf.transform(sensor, sensorML);
			System.out.println(sensorOWL);
		}
	}
}
