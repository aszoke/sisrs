package hu.bme.mit.cps.store.transform.xquery;

import hu.bme.mit.cps.store.SensorML2OWLTest;
import hu.bme.mit.cps.store.transform.TransformSensorML2OWL;

public class TransformSensorMLXQueryTest extends SensorML2OWLTest {

	@Override
	protected TransformSensorML2OWL setUpTransform() throws Exception {
		return new TransformSensorMLXQuery();
	}
}
