package hu.fandrew.stardog.bugs;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openrdf.model.Literal;
import org.openrdf.model.Resource;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;

import com.complexible.stardog.sesame.StardogRepository;

public class SesameAPITest {

	static Repository repo;
	static RepositoryConnection conn;
	static URI S, P, O, G;
	static Literal D;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		repo = new StardogRepository("http://localhost/TEST");
		repo.initialize();
		conn = repo.getConnection();
		conn.setNamespace("f", "http://fandrew/#");

		final ValueFactory factory = repo.getValueFactory();
		S = factory.createURI("f:S");
		P = factory.createURI("f:P");
		O = factory.createURI("f:O");
		D = factory.createLiteral(1);
		G = factory.createURI("f:G");

		conn.begin();
		conn.clear();
		conn.add(S, P, O);
		conn.add(S, P, D);
		conn.add(S, P, O, G);
		conn.add(S, P, D, G);
		conn.commit();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		conn.close();
	}

	// RepositoryConnection.size(Resource... contexts) is unsupported if context
	// is specified.
	@Test
	public void testSize() throws RepositoryException {
		assertEquals(4, conn.size());
		assertEquals(2, conn.size(G));
	}

	// RepositoryConnection.getContextIDs() returns context multiple times,
	// presumably for each statement it contains.
	@Test
	public void testGetContextIDs() throws RepositoryException {
		@SuppressWarnings("deprecation")
		final List<Resource> ctxs = conn.getContextIDs().asList();
		assertTrue(ctxs.contains(G));
		assertEquals(1, ctxs.size());
	}
}
