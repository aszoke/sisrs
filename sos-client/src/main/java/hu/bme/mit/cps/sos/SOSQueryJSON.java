package hu.bme.mit.cps.sos;

import hu.bme.mit.cps.sos.http.CachingHttpClient;

import java.io.IOException;
import java.net.URI;
import java.util.Set;

import org.apache.http.client.ClientProtocolException;
import org.apache.xmlbeans.XmlException;

import com.google.gson.Gson;

public class SOSQueryJSON {
	
	final URI jsonURI;
	CachingHttpClient chc = new CachingHttpClient();
	
	Set<String> sensors = null;
	
	public SOSQueryJSON(URI webappURI) {
		jsonURI = webappURI.resolve("./sos/json");
	}
	
	static class SOS20DescribeSensor {
		final String request = "DescribeSensor";
		final String service = "SOS";
		final String version = "2.0.0";
		final String procedure;
		final String procedureDescriptionFormat = "http://www.opengis.net/sensorML/1.0.1";
		
		SOS20DescribeSensor(String procedure) {
			this.procedure = procedure;
		}
	}
	
	static class SOS20DescribeSensorResponseProcDesc {
		String description;
	}
	
	static class SOS20DescribeSensorResponse {
		SOS20DescribeSensorResponseProcDesc procedureDescription;
	}
	
	public String x20DescribeSensor(String sensorID) throws ClientProtocolException, IOException, XmlException {
		SOS20DescribeSensor query = new SOS20DescribeSensor(sensorID);
		Gson gson = new Gson();
		String req = gson.toJson(query);
		String resp = chc.post(jsonURI, req, true);
		SOS20DescribeSensorResponse response = gson.fromJson(resp, SOS20DescribeSensorResponse.class);
		return response.procedureDescription.description;
	}
}
