package hu.bme.mit.cps.util;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class FileUtil {

	public static String readFile(String path, Charset encoding) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return encoding.decode(ByteBuffer.wrap(encoded)).toString();
	}

	public static void writeFile(String path, String content, Charset encoding) throws IOException {
		ByteBuffer encoded = encoding.encode(content);
		final Path p = Paths.get(path);
		Files.createDirectories(p.getParent());
		Files.write(p, encoded.array(), StandardOpenOption.CREATE);
	}
}
