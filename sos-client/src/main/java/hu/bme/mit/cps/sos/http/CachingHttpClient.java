package hu.bme.mit.cps.sos.http;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.ProtocolVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.util.EntityUtils;
import org.n52.oxf.util.web.HttpClientException;

public class CachingHttpClient extends N52AbstractHttpClient {

	static MessageDigest SHA;
	static {
		try {
			SHA = MessageDigest.getInstance("SHA");
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}
	
	final File cacheDirectory;
	final DefaultHttpClient client;
	
	public CachingHttpClient() {
		this(new File("cache/"));
	}

	public CachingHttpClient(File cacheDirectory) {
		super(new DefaultHttpClient());
		this.cacheDirectory = cacheDirectory;
		client = getHttpClientToDecorate();
	}

	String requestToHash(HttpUriRequest req) throws ParseException, IOException {
		SHA.reset();
		// add URI
		final String uri = req.getURI().toString();
		SHA.update(uri.getBytes(StandardCharsets.UTF_8));
		if (req instanceof HttpGet) {
		} else if (req instanceof HttpPost) {
			// add entity content
			HttpPost post = (HttpPost)req;
			if (!post.getEntity().isRepeatable()) {
				System.err.println("WARN: could not cache request, POST content not repeatable.");
				return null;
			}
			String content = EntityUtils.toString(post.getEntity());
			SHA.update(content.getBytes(StandardCharsets.UTF_8));
		} else {
			throw new IllegalArgumentException();
		}
		return Hex.encodeHexString(SHA.digest());
	}
	
	File hashToFile(String sha) {
		return new File(cacheDirectory, sha);
	}

	String executeCached(HttpUriRequest req, String sha) throws IOException {
		final File cacheFile = hashToFile(sha);
		if (!cacheFile.exists()) {
			return null;
		}
		return FileUtils.readFileToString(cacheFile, StandardCharsets.UTF_8);
	}
	
	void saveCached(HttpUriRequest req, String sha, String content) throws IOException {
		final File cacheFile = hashToFile(sha);
		FileUtils.write(cacheFile, content, StandardCharsets.UTF_8);
	}
	
	public String get(URI uri) throws ClientProtocolException, IOException {
		HttpUriRequest get = new HttpGet(uri);
		return execute(get);
	}
	
	public String post(URI uri, String content, boolean json) throws ClientProtocolException, IOException {
		HttpPost post = new HttpPost(uri);
		if (json) {
			post.setHeader("Content-Type", "application/json");
			post.setHeader("Accept", "application/json");
		}
		post.setEntity(new StringEntity(content));
		return execute(post);
	}
	
	public String post(URI uri, String content) throws ClientProtocolException, IOException {
		return post(uri, content, false);
	}
	
	public String execute(HttpUriRequest req) throws IOException {
		String res;
		final String sha = requestToHash(req);
		if ((res = executeCached(req, sha)) != null) {
			System.err.println("C " + req + " " + sha);
			return res;
		}
		HttpResponse resp = client.execute(req);
		res = EntityUtils.toString(resp.getEntity());
		saveCached(req, sha, res);
		System.err.println("D " + req + " " + sha);
		return res;
	}

	// N52AbstractHttpClient
	@Override
	public HttpResponse executeMethod(HttpRequestBase method)
			throws HttpClientException {
		try {
			String result = execute(method);
			HttpResponse resp = new BasicHttpResponse(new ProtocolVersion("HTTP", 1, 0), 200, "OK");
			resp.setEntity(new StringEntity(result));
			return resp;
		} catch (IOException e) {
			throw new HttpClientException("HTTP error", e);
		}
	}
}
