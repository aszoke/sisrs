package hu.bme.mit.cps.sos;

import hu.bme.mit.cps.sos.http.CachingHttpClient;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

import net.opengis.ows.x11.AllowedValuesDocument.AllowedValues;
import net.opengis.ows.x11.DomainType;
import net.opengis.ows.x11.OperationDocument;
import net.opengis.ows.x11.OperationsMetadataDocument.OperationsMetadata;
import net.opengis.ows.x11.SectionsType;
import net.opengis.ows.x11.ValueType;
import net.opengis.sensorML.x101.IdentificationDocument;
import net.opengis.sensorML.x101.IdentificationDocument.Identification;
import net.opengis.sensorML.x101.SensorMLDocument;
import net.opengis.sensorML.x101.SensorMLDocument.SensorML;
import net.opengis.sos.x10.DescribeSensorDocument;
import net.opengis.sos.x10.DescribeSensorDocument.DescribeSensor;
import net.opengis.sos.x20.CapabilitiesDocument;
import net.opengis.sos.x20.CapabilitiesType;
import net.opengis.sos.x20.GetCapabilitiesDocument;
import net.opengis.sos.x20.GetCapabilitiesType;
import net.opengis.swes.x20.DescribeSensorResponseDocument;
import net.opengis.swes.x20.DescribeSensorResponseType;
import net.opengis.swes.x20.SensorDescriptionType;

import org.apache.http.client.ClientProtocolException;
import org.apache.xmlbeans.XmlException;
import org.n52.oxf.OXFException;
import org.n52.oxf.adapter.OperationResult;
import org.n52.oxf.adapter.ParameterContainer;
import org.n52.oxf.ows.ExceptionReport;
import org.n52.oxf.ows.capabilities.Operation;
import org.n52.oxf.sos.adapter.ISOSRequestBuilder;
import org.n52.oxf.sos.adapter.SOSAdapter;

public class SOSQuery {
	
	final URI sosURI;
	CachingHttpClient chc = new CachingHttpClient();
	
	Set<String> sensors = null;
	
	public SOSQuery(URI webappURI) {
		sosURI = webappURI.resolve("./sos/pox");
	}
	
	public OperationsMetadata getOperationsMetadata() throws IOException, XmlException {
		GetCapabilitiesDocument gcd = GetCapabilitiesDocument.Factory.newInstance();
		GetCapabilitiesType gct = gcd.addNewGetCapabilities2();
		SectionsType st = gct.addNewSections();
		st.addSection("OperationsMetadata");
//		System.out.println("REQUEST[getCapabilities/OperationsMetadata]<<\n" + gcd.toString() + ">>\n");
		String response = chc.post(sosURI, gcd.toString());
		
		CapabilitiesDocument cd = CapabilitiesDocument.Factory.parse(response);
		CapabilitiesType c = cd.getCapabilities();
		return c.getOperationsMetadata();
	}
	
	public Set<String> getSensors() throws IOException, XmlException {
		OperationsMetadata om = getOperationsMetadata();
		for (OperationDocument.Operation o : om.getOperationArray()) {
			if ("DescribeSensor".equals(o.getName())) {
				for (DomainType dt : o.getParameterArray()) {
					if ("procedure".equals(dt.getName())) {
						sensors = new TreeSet<>();
						AllowedValues avs = dt.getAllowedValues();
						for (ValueType vt : avs.getValueArray()) {
							sensors.add(vt.getStringValue());
						}
					}
				}
			}
		}
		if (sensors == null) {
			throw new RuntimeException(
					"Could not find list of sensors in getCapabilities/Operation[DescribeSensor]/Parameter[procedure].");
		} else {
			return Collections.unmodifiableSet(sensors);
		}
	}
	
	// SOS 2.0->1.0 conversion fails in SOS server 4.0
	@Deprecated
	public String describeSensor(String sensorID) throws ClientProtocolException, IOException, XmlException {
		// SOS 1.0.0, not implemented for 2.0.0
		DescribeSensorDocument dsd = DescribeSensorDocument.Factory.newInstance();
		DescribeSensor ds = dsd.addNewDescribeSensor();
		ds.setProcedure(sensorID);
		ds.setService("SOS");
		ds.setVersion("1.0.0");
		ds.setOutputFormat("text/xml;subtype=\"sensorML/1.0.1\"");
//		System.out.println("REQUEST[describeSensor]<<\n" + dsd.toString() + ">>\n");
		return chc.post(sosURI, dsd.toString());
	}
	
	public void dumpSensorMLDocument(String source) throws XmlException {
		SensorMLDocument smld = SensorMLDocument.Factory.parse(source);
		SensorML sml = smld.getSensorML();
		System.err.println("SML: " + sml.toString());
		System.err.println("ValidTime: " + sml.getValidTime());
		System.err.println("MEMBERs: " + sml.getMemberArray().length);
		System.err.println("IDENTIFICATIONs: " + sml.getIdentificationArray().length);
		for (Identification id0 : sml.getIdentificationArray()) {
			for (IdentificationDocument.Identification.IdentifierList.Identifier id : id0.getIdentifierList().getIdentifierArray()) {
				System.err.println(id.getTerm().getDefinition() + "|" + id.getTerm().getValue());
			}
		}
		SensorML.Member m = sml.getMemberArray(0);
		System.err.println("MEMBER[0]: " + m);
	}
	
	public OperationResult describeSensorOFX(String sensor) throws ExceptionReport, OXFException, XmlException, IOException {
		SOSAdapter sos = new SOSAdapter("2.0.0");
		sos.setHttpClient(chc);
//		ServiceDescriptor sd = sos.initService(SOS_SERVICE_URI.toString());
		Operation operation = new Operation(
				org.n52.oxf.sos.adapter.SOSAdapter.DESCRIBE_SENSOR, sosURI.toString() + "?", sosURI.toString());
		ParameterContainer parameters = new ParameterContainer();
		parameters.addParameterShell(ISOSRequestBuilder.DESCRIBE_SENSOR_SERVICE_PARAMETER, "SOS");
		parameters.addParameterShell(ISOSRequestBuilder.DESCRIBE_SENSOR_VERSION_PARAMETER, "2.0.0");
		parameters.addParameterShell(ISOSRequestBuilder.DESCRIBE_SENSOR_PROCEDURE_PARAMETER, sensor);
		parameters.addParameterShell(ISOSRequestBuilder.DESCRIBE_SENSOR_PROCEDURE_DESCRIPTION_FORMAT, "text/xml;subtype=\"sensorML/2.0.0\"");
		
		return sos.doOperation(operation, parameters);
	}
	
	public void dumpDescribeSensorResponseDocument(InputStream is) throws XmlException, IOException {
		DescribeSensorResponseDocument dsrd = DescribeSensorResponseDocument.Factory.parse(is);
		DescribeSensorResponseType dsrt = dsrd.getDescribeSensorResponse();
		assert dsrt.getDescriptionArray().length == 1;
		DescribeSensorResponseType.Description d = dsrt.getDescriptionArray()[0];
		SensorDescriptionType.Data sdtd = d.getSensorDescription().getData();
		System.err.println(sdtd);
	}
}
