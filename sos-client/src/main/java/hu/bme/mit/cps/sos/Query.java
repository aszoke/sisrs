package hu.bme.mit.cps.sos;

import hu.bme.mit.cps.util.FileUtil;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

public class Query {

	// TODO: put into cli options
	public final static boolean IGNORE_CACHE = true;
	public final static String EXPORT_LOCATION = "export/";
	// TODO: move to config file
	public final static URI TEST_SOS35_SERVER_URI = URI
			.create("http://152.66.253.152:8080/52nSOSv3.5.0/sos"); // 52°North SOS 3.5
	public final static URI TEST_SOS40_SERVER_WEBAPP = URI
			.create("http://152.66.253.152:8080/52n-sos-webapp/"); // 52°North SOS 4.0
	
	public final static URI SOS_SERVER_ML_WEBAPP = URI.create("http://stardog:8080/52n-sos-webapp/");
	public final static URI SOS_SERVER_DEBUG = URI.create("http://localhost:5555/");
	
	// source: http://52north.org/communities/sensorweb/sos/
	public final static URI SOS_SERVER_Demo = URI
			.create("http://sensorweb.demo.52north.org/52n-sos-webapp/sos/pox");
	public final static URI SOS_SERVER_AirQuality = URI
			.create("http://geoviqua.dev.52north.org/SOS-Q/sos/pox");
	// no POX binding
	public final static URI SOS_SERVER_Hydrology = URI
			.create("http://geowow.dev.52north.org/52n-sos-grdc-webapp/sos");
	
	
	public final static String PRINT_LIST_PREFIX = "  - ";

	public static void main(String args[]) throws Exception {
		//final URI webapp = TEST_SOS40_SERVER_WEBAPP;
		final URI webapp = SOS_SERVER_ML_WEBAPP;
		
		SOSQuery sq = new SOSQuery(webapp);
		SOSQueryJSON sqj = new SOSQueryJSON(webapp);
		
		//System.out.println(sqj.describeSensor_SOS20_JSON("http://www.52north.org/test/procedure/9"));
		//System.exit(0);
		
		Set<String> sensors = sq.getSensors();
		System.out.println("Sensors:\n" + PRINT_LIST_PREFIX
				+ StringUtils.join(sensors, "\n" + PRINT_LIST_PREFIX));
		
		if (EXPORT_LOCATION != null) {
			// create export directory
			Files.createDirectories(Paths.get(EXPORT_LOCATION));
		}
		for (String sensor : sensors) {
			System.out.println("Querying sensor: " + sensor);
			String sensorMLDocument = sqj.x20DescribeSensor(sensor);
			if (EXPORT_LOCATION != null) {
				String filename = EXPORT_LOCATION + sensor.substring(sensor.lastIndexOf('/')+1).replace(':', '_') + ".xml";
				System.out.println("  writing response to '" + filename + "'");
				FileUtil.writeFile(filename, sensorMLDocument, StandardCharsets.UTF_8);
			}
			
//			sq.dumpSensorMLDocument(sensorMLDocument);

//			OperationResult res = sq.describeSensorOFX(sensor);
//			sq.dumpDescribeSensorResponseDocument(res.getIncomingResultAsStream());
		}
	}
}
